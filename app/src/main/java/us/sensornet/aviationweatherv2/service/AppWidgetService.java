/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 11/10/18 5:36 PM
 */

package us.sensornet.aviationweatherv2.service;

import android.appwidget.AppWidgetManager;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.AirportRepository;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Barometer;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Cloud;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Temperature;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Visibility;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Wind;
import us.sensornet.aviationweatherv2.network.ResponseEnvelope;
import us.sensornet.aviationweatherv2.utils.FlightRulesToColor;

import static us.sensornet.aviationweatherv2.ui.adapter.AviationWeatherAppWidgetProvider.EXTRA_DATA;
import static us.sensornet.aviationweatherv2.ui.adapter.AviationWeatherAppWidgetProvider.EXTRA_ITEM_ICAO_CODE;
import static us.sensornet.aviationweatherv2.ui.adapter.AviationWeatherAppWidgetProvider.EXTRA_ITEM_POSITION;

public class AppWidgetService extends RemoteViewsService {
    private static final String TAG = "AppWidgetService";
    
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        
        return new AppWidgetItemFactory(getApplicationContext(), intent);
    }

    class AppWidgetItemFactory implements RemoteViewsFactory {
        private static final String TAG = "AppWidgetItemFactory";

        private Context context;
        private int appWidgetId;
        private AirportRepository mRepo;
        private List<MetarReport> metarReports = new ArrayList<>();
        private LiveData<ResponseEnvelope<List<MetarReport>>> responseEnvelope;
        private MetarReport[] mReports;
        
        public AppWidgetItemFactory(Context context, Intent intent) {
            Log.d(TAG, "AppWidgetItemFactory: called");
            this.context = context;
            appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            Gson gson = new Gson();
            String jsonString = intent.getStringExtra(EXTRA_DATA);
            mReports = gson.fromJson(jsonString, MetarReport[].class);
            Log.d(TAG, "AppWidgetItemFactory: Reports: " + mReports.toString());
            mRepo = AirportRepository.getInstance(getApplication());
        }

        @Override
        public void onCreate() {
            Log.d(TAG, "onCreate: called");
            // Connect to data source
            mRepo.getMetarReports(null).observeForever(new Observer<ResponseEnvelope<List<MetarReport>>>() {
                @Override
                public void onChanged(@Nullable ResponseEnvelope<List<MetarReport>> listResponseEnvelope) {
                    responseEnvelope = listResponseEnvelope;
                    getDataFromEnvelope(listResponseEnvelope);
                    //onDataSetChanged();
                }
            });

        }


        private void getDataFromEnvelope(ResponseEnvelope<List<MetarReport>> response) {
            Log.d(TAG, "getDataFromEnvelope: called");
            // If we got an error return
            if(response.getIsError())
                return;
            // If we're here we have no errors so, process
            Log.d(TAG, "getDataFromEnvelope: updating metarReports");
            for(MetarReport report: response.getData()) {
                metarReports.add(report);
            }
        }

        @Override
        public void onDataSetChanged() {
            Log.d(TAG, "onDataSetChanged: called");
            if(null == metarReports) {
                metarReports = new ArrayList<>();
            }

            if(null != mReports) {
                Log.d(TAG, "AppWidgetItemFactory: called generating reports");
                metarReports.clear();
                for (int i = 0; i < mReports.length - 1; i++) {
                    metarReports.add(mReports[i]);
                }
            } else {
                Log.d(TAG, "AppWidgetItemFactory: called reports is null");
            }


        }

        @Override
        public void onDestroy() {
            // Close data source
        }

        @Override
        public int getCount() {
            if(null == metarReports) {
                return 0;
            } else {
                return metarReports.size();
            }

        }


        @Override
        public RemoteViews getViewAt(int position) {
            Log.d(TAG, "getViewAt: position " + String.valueOf(position));

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.app_widget_item);
            MetarReport metarReport = metarReports.get(position);

            Intent fillInIntent = new Intent();
            fillInIntent.putExtra(EXTRA_ITEM_POSITION, position);
            fillInIntent.putExtra(EXTRA_ITEM_ICAO_CODE, metarReport.getIcao());
            views.setOnClickFillInIntent(R.id.app_widget_stack_view, fillInIntent);


            // Make it possible to distinguish the individual on-click
            // action of the given item
            views.setOnClickFillInIntent(R.id.widget_item_view,fillInIntent);
            // Make sure we have data to work with
            // Makes sure we have data to work with
            if ((position == AdapterView.INVALID_POSITION) || (null == metarReports) || (0 == metarReports.size())) {
                return null;
            }

            // Set item view values...
            try {

                String airportName = metarReport.getStation().getName();
                views.setTextViewText(R.id.tv_value_station_name, airportName);

                String icaoCode = metarReport.getIcao();
                views.setTextViewText(R.id.tv_value_icao_code, icaoCode);

                String flightRules = metarReport.getFlightCategory();
                views.setTextViewText(R.id.tv_value_flight_rules, flightRules);
                int mColor = getResources().getColor(android.R.color.white);
                views.setTextColor(R.id.tv_value_flight_rules, mColor);

                int flightRulesColor = FlightRulesToColor.getColor(flightRules);
                views.setInt(R.id.tv_value_flight_rules, "setBackgroundColor", flightRulesColor);

                views.setTextViewText(R.id.tv_value_wind_direction, getWindString(position));

                views.setTextViewText(R.id.tv_value_clouds, getCloudString(position));

                views.setTextViewText(R.id.tv_value_visibility, getVisibilityString(position));

                views.setTextViewText(R.id.tv_value_observed, getObservedString(position));

                views.setTextViewText(R.id.tv_value_temperature, getTemperatureString(position));

                views.setTextViewText(R.id.tv_value_pressure, getBarometricPressureString(position));


            } catch(Exception e) {
                Log.d(TAG, "getViewAt: Exception: " + e.getMessage());
            }
            return views;
        }


        private String getTemperatureString(int pos) {
            Temperature tempObj = metarReports.get(pos).getTemperature();
            String strTemp = tempObj.getFahrenheit() + "\u2109";
            return strTemp;
        }

        private String getBarometricPressureString(int pos) {
            Barometer barometer = metarReports.get(pos).getBarometer();
            String pressure = "";
            pressure = pressure.concat(" "
                    + barometer.getHg()
                    + " ");
            return pressure;
        }

        private String getObservedString(int pos) {
            String observationTime = getResources().getString(R.string.tv_label_observation_text);
            observationTime = observationTime.concat(metarReports.get(pos).getObserved());
            return observationTime;

        }

        private String getVisibilityString(int pos) {
            Visibility visibilityObj = metarReports.get(pos).getVisibility();
            return (getResources().getString(R.string.visibility)
                + " "
                + visibilityObj.getMiles()
                + " "
                + getResources().getString(R.string.miles));
        }

        private String getWindString(int pos) {
            Wind windObj = metarReports.get(pos).getWind();
            String strWind = getResources().getString(R.string.tv_label_wind_text)
                    + ": "
                    + windObj.getDegrees() + "\u00B0"
                    + " "
                    + (windObj.getSpeedMph().toString().isEmpty() ?
                    " " : windObj.getSpeedMph()
                    + " "
                    + getResources().getString(R.string.mph));
            return strWind;
        }

        private String getCloudString(int pos) {
            List<Cloud> clouds =  metarReports.get(pos).getClouds();
            String strClouds = getResources().getString(R.string.clouds);
            strClouds = strClouds.concat(" ");

            for(Cloud cloud: clouds){
                strClouds = strClouds.concat(
                     cloud.getCode() + " "
                     + getResources().getString(R.string.at)
                     + " "
                     + cloud.getBaseFeetAgl()
                     +"\n");
            }
            strClouds = strClouds.substring(0, strClouds.length()-1);
            return strClouds;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }


    }
}
