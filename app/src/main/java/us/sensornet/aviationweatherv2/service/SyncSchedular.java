/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 11/6/18 8:26 PM
 */

package us.sensornet.aviationweatherv2.service;


import android.content.Context;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import us.sensornet.aviationweatherv2.database.AirportRepository;

// This class provides a jobschedular that calls
// the api to refresh data every 30 minutes.
public class SyncSchedular extends JobService {
    private static final String TAG = "SyncSchedular";

    private final String CONTEXT_KEY = "CONTEXT";

    private Context mContext;
    private AirportRepository mRepo;


    @Override
    public boolean onStartJob(JobParameters job) {
        // We call the repo method and let the observers
        // notify everyone of the new data
        Log.d(TAG, "onStartJob: called and Entered");
        mRepo = AirportRepository.getInstance(getApplication());
        Log.d(TAG, "onStartJob: calling Repo.getWatchedAirports");
        mRepo.getMetarReports(null);
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        Log.d(TAG, "onStopJob: called");
        return false;
    }
}
