/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/27/18 12:44 PM
 */

package us.sensornet.aviationweatherv2.utils;

import android.app.Application;
import android.os.Build;

import us.sensornet.aviationweatherv2.R;

public class FlightRulesToColor {

    private static Application mApplication;

    public FlightRulesToColor(Application application) {
        mApplication = application;
    }


    public static int getColor(String flightRules) {

        if (Build.VERSION.SDK_INT >= 23) {
            switch (flightRules.toLowerCase()) {
                case "vfr":
                    return mApplication.getColor(R.color.colorVFR);

                case "mvfr":
                    return mApplication.getColor(R.color.colorMVFR);

                case "ifr":
                    return mApplication.getColor(R.color.colorIFR);

                case "lifr":
                    return mApplication.getColor(R.color.colorLIFR);

                default:
                    return mApplication.getColor(R.color.colorUnknown);
            }
        } else {
            // Older device
            switch (flightRules.toLowerCase()) {
                case "vfr":
                    return mApplication.getApplicationContext().getResources().getColor(R.color.colorVFR);

                case "mvfr":
                    return mApplication.getApplicationContext().getResources().getColor(R.color.colorMVFR);

                case "IFR":
                    return mApplication.getApplicationContext().getResources().getColor(R.color.colorIFR);

                case "LIFR":
                    return mApplication.getApplicationContext().getResources().getColor(R.color.colorLIFR);

                default:
                    return mApplication.getApplicationContext().getResources().getColor(R.color.colorUnknown);
            }
        }
    }
}
