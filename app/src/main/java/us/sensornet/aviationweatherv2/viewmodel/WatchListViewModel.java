/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/21/18 4:21 PM
 */

package us.sensornet.aviationweatherv2.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import us.sensornet.aviationweatherv2.database.AirportRepository;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.AppDatabase;
import us.sensornet.aviationweatherv2.database.Weather.metar.MetarObjectEntity;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListEntity;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.network.ResponseEnvelope;

public class WatchListViewModel extends AndroidViewModel {
    private static final String TAG = "WatchListViewModel";

    public LiveData<List<WatchListEntity>> mWatchList;
    public LiveData<List<AirportEntity>> mAirports;

    // From API
    public LiveData<ResponseEnvelope<List<MetarReport>>> mMetarReports;// = new MutableLiveData<>();

    // Stored locally
    public LiveData<MetarReport> currentStationReport;


    private AppDatabase mDatabase;

    private Application mApplication;

    private AirportRepository mRepository;


    public WatchListViewModel(@NonNull Application application) {
        super(application);
        mRepository = AirportRepository.getInstance(application);
        mWatchList = mRepository.getAllWatched();
    }

    public LiveData<List<WatchListEntity>> get() {
        mWatchList = mRepository.getAllWatched();
        return mWatchList;
    }

    public LiveData<List<AirportEntity>> getWatchedAirports() {
        mAirports = mRepository.getWatchedAirports();
        return mAirports;
    }

    public void insert(WatchListEntity entity) {
        mRepository.insertWatched(entity);
    }

    public void insertWatchedAirport(AirportEntity airport) {
        WatchListEntity entity = new WatchListEntity();
        entity.setAirport_id(airport.getId());
        mRepository.insertWatched(entity);
    }


    public void update(WatchListEntity entity) {
        mRepository.updateWatched(entity);
    }

    public void delete(WatchListEntity entity) {
        mRepository.deleteWatched(entity);
    }

    public void deleteWatchedAirport(AirportEntity airport) {
        mRepository.deleteWatchedAirport(airport);
    }


    public LiveData<ResponseEnvelope<List<MetarReport>>> getMetarReport(String icao_list) {
        Log.d(TAG, "getMetarReport: called with icao_list: " + icao_list);
        mRepository.getMetarReports(icao_list).observeForever(new Observer<ResponseEnvelope<List<MetarReport>>>() {
            @Override
            public void onChanged(@Nullable ResponseEnvelope<List<MetarReport>> listResponseEnvelope) {
                mMetarReports = listResponseEnvelope;
            }
        });
        Log.d(TAG, "getMetarReport: called, returning response");
        //Log.d(TAG, "getMetarReport: MetarReports: " + mMetarReports.getValue().getData().get(0).getIcao());
        return mMetarReports;
    }


    public LiveData<MetarObjectEntity> getCurrentStationReport(String icao) {
        return mRepository.getStationMetarReport(icao);
    }

}
