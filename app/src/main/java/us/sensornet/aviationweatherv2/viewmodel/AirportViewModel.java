/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/21/18 6:00 PM
 */

package us.sensornet.aviationweatherv2.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import us.sensornet.aviationweatherv2.database.AirportRepository;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;

public class AirportViewModel extends AndroidViewModel {
    private static final String TAG = "AirportViewModel";
    private static Integer startAt = 0;
    private static Integer maxCount = 0;
    public LiveData<List<AirportEntity>> mAirports;
    private MutableLiveData<String> filter = new MutableLiveData<String>();
    private AirportRepository mRepository;

    private Application mApplication;


    public AirportViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
        mRepository = AirportRepository.getInstance(mApplication);
        mAirports = mRepository.getPagedAirports(startAt);
        if (null != mAirports.getValue()) {
            maxCount = mAirports.getValue().size();
        } else {
            maxCount = 0;
        }

        if (null == ((LiveData<String>) filter).getValue()) {
            String local = application.getResources().getConfiguration().locale.getCountry();
            filter.setValue(local);
        }

        Log.d(TAG, "AirportViewModel: Trasformation.switchMap() called");
        mAirports = Transformations.switchMap(filter,
                (value) -> mRepository.getMatching(("%" + value + "%")));
        Log.d(TAG, "AirportViewModel: Transformation.switchMap() completed");
    }

    // Airport Methods
    public void insert(List<AirportEntity> airports) {
        mRepository.insert(airports);
    }

    public void update(List<AirportEntity> airports) {
        mRepository.update(airports);
    }

    public void delete(List<AirportEntity> airports) {
        mRepository.delete(airports);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    public AirportEntity getOldest() {
        return mRepository.oldest();
    }

    public int countAll() {
        return mRepository.count();
    }


    public void setFilter(String filter) {
        Log.d(TAG, "setFilter: called");
        this.filter.setValue(filter);
        Log.d(TAG, "AirportViewModel: Trasformation.switchMap() called");
    }

    public LiveData<List<AirportEntity>> getAirports() {
        //Log.d(TAG, "getAirports: called, returning " + String.valueOf(mAirports.getValue().size()) + " records in mAirports");
        return mAirports;
    }

    public LiveData<List<AirportEntity>> getAirportsMatching() {
        // Add wild cards to string
        String key = filter.getValue();
        key = "%" + key + "%";
        Log.d(TAG, "getAirportsMatching: called, querying mRepository with key: " + key);
        return mRepository.getMatching(key);
    }

}
