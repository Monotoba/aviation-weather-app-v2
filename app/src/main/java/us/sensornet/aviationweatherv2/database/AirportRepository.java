/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/23/18 2:26 PM
 */

package us.sensornet.aviationweatherv2.database;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import us.sensornet.aviationweatherv2.database.Airports.AirportDao;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.Weather.metar.MetarObjectEntity;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListDao;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListEntity;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.network.CheckWXApiProxy;
import us.sensornet.aviationweatherv2.network.ResponseEnvelope;

public class AirportRepository implements CheckWXApiProxy.CheckWXResponseInterface {
    private static final String TAG = "AirportRepository";
    private static AirportRepository INSTANCE;
    private static CheckWXApiProxy mApiProxy = null;
    private int mAirportCount = 0;
    private AirportEntity mOldestAirport = new AirportEntity();
    private AirportDao mAirportDao;
    private LiveData<List<AirportEntity>> mAirports;
    private int mWatchedCount = 0;
    private LiveData<List<WatchListEntity>> mWatchList;
    private WatchListDao mWatchDao;
    private CheckWXApiProxy mWXProxy;
    private MutableLiveData<ResponseEnvelope<List<MetarReport>>> mMetarReports = new MutableLiveData<>();
    private LiveData<MetarObjectEntity> mMetarReportObject;
    private Application mApplication;
    private AppDatabase mDatabase;
    private String strIcaoList;

    private AirportRepository(Application mApplication) {
        this.mApplication = mApplication;
        mDatabase = AppDatabase.getInstance(mApplication, null);
        mAirportDao = mDatabase.airportDao();
        mAirports = mAirportDao.get(0);
        mWatchDao = mDatabase.watchListDao();
        mWatchList = mWatchDao.getAll();

        if (null == mAirports.getValue() || mAirports.getValue().size() <= 0) {
            // we have no airports in database
            // So add them
            //importCsvFile();
        }

        mWXProxy = CheckWXApiProxy.getInstance(mApplication);
        mWXProxy.setListener(this);
    }

    // Users should get the Repo via getInstance
    public static AirportRepository getInstance(Application application) {
        if (null == INSTANCE) {
            INSTANCE = new AirportRepository(application);
        }

        return INSTANCE;
    }


    public void setIcaoList(String icaoList) {
        this.strIcaoList = icaoList;
    }

    /**
     * Tests data for age. If older than
     * 14 days, requests data from API
     * and uses that data to refresh db.
     *
     * @return boolean
     */
    public void verifyData() {
        // Call database methods to get data
        this.oldest(); // updates mOldestAirport
        this.count();
    }


    // get airports
    public LiveData<List<AirportEntity>> getPagedAirports(int starting_from) {
        mAirports = mDatabase.airportDao().get(starting_from);
        return mAirports;
    }


    // get matching
    public LiveData<List<AirportEntity>> getMatching(String key) {
        mAirports = mDatabase.airportDao().getMatching(key);
        return mAirports;
    }


    public LiveData<List<AirportEntity>> getAirports() {
        return mAirports;
    }

    // Insert
    public void insert(List<AirportEntity> airports) {
        new InsertAirportsAsyncTask(mAirportDao).execute(airports);
    }

    // Update
    public void update(List<AirportEntity> airports) {
        new UpdateAirportsAsyncTask(mAirportDao).execute(airports);
    }

    // delete
    public void delete(List<AirportEntity> airports) {
        new InsertAirportsAsyncTask(mAirportDao).execute(airports);
    }

    // delete all
    public void deleteAll() {
        new InsertAirportsAsyncTask(mAirportDao).execute();
    }

    // get oldest
    // Note this is a blocking call however, the results
    // are used in a sequential process.
    public AirportEntity oldest() {
        AirportEntity airport = new AirportEntity();

        OldestAirportsAsyncTask task = new OldestAirportsAsyncTask(mAirportDao);
        try {
            mOldestAirport = task.get(10, TimeUnit.MICROSECONDS);
        } catch (Exception e) {
            Log.d(TAG, "Could not retrieve oldest airport: " + e.getMessage());
        }

        return mOldestAirport;
    }

    // get count
    // Note this is a blocking call however, the results
    // are used in a sequential process.
    public int count() {
        CountAirportsAsyncTask task = new CountAirportsAsyncTask(mAirportDao);
        try {
            Log.d(TAG, "count: counting airports in database");
            mAirportCount = task.get(100, TimeUnit.MICROSECONDS);

        } catch (Exception e) {
            Log.d(TAG, "count: Could not count airports: " + e.getMessage());
        }

        return mAirportCount;
    }

    // Set isWatched
    public void setIsWatched(Integer id) {
        new SetIsWatchedAsyncTask(mDatabase.airportDao()).execute(id);
    }

    // Reset isWatched
    public void resetIsWatched(Integer id) {
        new ResetIsWatchedAsyncTask(mDatabase.airportDao()).execute(id);
    }

    /*
     *-------------------------------------------------------------
     * WatchList Methods
     *-------------------------------------------------------------
     */

    public LiveData<List<WatchListEntity>> getAllWatched() {
        mWatchList = mDatabase.watchListDao().getAll();
        return mWatchList;
    }

    public LiveData<List<AirportEntity>> getWatchedAirports() {
        // Get airports from db.
        mAirports = mDatabase.watchListDao().getWatchedAitrports();

        return mAirports;
    }

    public LiveData<List<WatchListEntity>> getWatchedById(int id) {
        mWatchList = mDatabase.watchListDao().getById(id);
        return mWatchList;
    }

    public LiveData<List<WatchListEntity>> getWatchedByIcao(String icao) {
        if(null == icao)
            icao = strIcaoList;
        else
            strIcaoList = icao;

        Log.d(TAG, "getWatchedByIcao: called calling: " + icao);
        mWatchList = mDatabase.watchListDao().getWatchedAirportsByICAO(icao);
        return mWatchList;
    }

    public int getWatchedCount() {
        CountWatchedAsyncTask task = new CountWatchedAsyncTask(mDatabase.watchListDao());
        try {
            Log.d(TAG, "count: counting airports in database");
            mAirportCount = task.get(100, TimeUnit.MICROSECONDS);

        } catch (Exception e) {
            Log.d(TAG, "count: Could not count airports: " + e.getMessage());
        }

        return mAirportCount;
    }

    public void insertWatched(WatchListEntity entity) {
        Log.d(TAG, "insertWatched: called");

        List<WatchListEntity> entities = new ArrayList<>();
        entities.add(entity);
        this.setIsWatched(entity.getAirport_id());
        new InsertWatchedAsyncTask(mDatabase.watchListDao()).execute(entities);
    }

    public void updateWatched(WatchListEntity entity) {
        new UpdateWatchedAsyncTask(mDatabase.watchListDao()).execute(entity);
    }

    public void deleteWatched(WatchListEntity entity) {
        Log.d(TAG, "deleteWatched: called");
        this.resetIsWatched(entity.getAirport_id());
        new DeleteWatchedAsyncTask(mDatabase.watchListDao()).execute(entity);

    }

    public void deleteWatchedAirport(AirportEntity entity) {
        Log.d(TAG, "deleteWatched: called");
        this.resetIsWatched(entity.getId());
        new DeleteWatchedAirportAsyncTask(mDatabase.watchListDao()).execute(entity);

    }

    public void deleteAllWatched() {
        new DeleteAllWatchedAsyncTask(mDatabase.watchListDao()).execute();
    }


    /*
    mViewModel.getWatchedAirports().observe(this, new Observer<List<AirportEntity>>() {
        @Override
        public void onChanged(@Nullable List<AirportEntity> airportEntities) {

            if (airportEntities.size() > 0) {
                showWacthListFragment();
            } else {
                // Add the place holder view
                showNoStationsSelected();
            }
        }
    });
    */

    public LiveData<ResponseEnvelope<List<MetarReport>>> getMetarReports(String icao_list) {
        Log.d(TAG, "getMetarReports: Repository requestng API Metar Reports for: " + icao_list);
        if(null == icao_list)
            icao_list = strIcaoList;
        else
            strIcaoList = icao_list;

        Log.d(TAG, "getMetarReports: called: calling with: " + icao_list);
        mWXProxy.getMetarReport(icao_list).observeForever(new Observer<ResponseEnvelope<List<MetarReport>>>() {
            @Override
            public void onChanged(@Nullable ResponseEnvelope<List<MetarReport>> listResponseEnvelope) {
                Log.d(TAG, "onChanged: in Repository called");
                if(listResponseEnvelope.getIsError()) {
                    // handle errors
                } else {
                    if(listResponseEnvelope.getData().size() > 0)
                    mMetarReports.setValue(listResponseEnvelope);
                }
            }
        });
        return mMetarReports;
    }

    @Override
    public void onMetarResponse(ResponseEnvelope<List<MetarReport>> envelope) {
        if (envelope.getIsError()) {
            // We have an error object. What to do?
        } else {
            // We have data
            Log.d(TAG, "onMetarResponse: API Received Metar Reports");
            saveMetarEnvelope(envelope);
            mMetarReports.setValue(envelope);

        }
    }

    private void saveMetarEnvelope(ResponseEnvelope<List<MetarReport>> envelope) {
        Gson gson = new Gson();
        List<MetarReport> reports = envelope.getData();
        List<MetarObjectEntity> metarObjects = new ArrayList<>();

        for (MetarReport report : reports) {
            String sObject = gson.toJson(report);
            MetarObjectEntity metarObject = new MetarObjectEntity(report.getIcao(), sObject, report.getObserved());
            Log.d(TAG, "onMetarResponse: Envelope: " + sObject);
            metarObjects.add(metarObject);
        }

        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            Log.d(TAG, "saveMetarEnvelope: Repsoitory is saving reports to database");
            mDatabase.metarObjectDao().insert(metarObjects);
        });
    }

    public LiveData<MetarObjectEntity> getStationMetarReport(String icao) {
        Gson gson = new Gson();
        mMetarReportObject = mDatabase.metarObjectDao().getByIcao(icao);
        return mMetarReportObject;
    }

    /**
     * The following method provide AsyncTask
     * wrapper classes to handle our database
     * queries on background threads
     */

    // Insert
    private static class InsertAirportsAsyncTask extends AsyncTask<List<AirportEntity>, Void, Void> {
        private AirportDao dao;

        public InsertAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(List<AirportEntity>... lists) {
            dao.insert(lists[0]);
            return null;
        }
    }

    // Update
    private static class UpdateAirportsAsyncTask extends AsyncTask<List<AirportEntity>, Void, Void> {
        private AirportDao dao;

        public UpdateAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(List<AirportEntity>... lists) {
            dao.update(lists[0]);
            return null;
        }
    }

    // delete
    private static class DeleteAirportsAsyncTask extends AsyncTask<List<AirportEntity>, Void, Void> {
        private AirportDao dao;

        public DeleteAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(List<AirportEntity>... lists) {
            dao.update(lists[0]);
            return null;
        }
    }

    // delete all
    private static class DeleteAllAirportsAsyncTask extends AsyncTask<Void, Void, Void> {
        private AirportDao dao;

        public DeleteAllAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(Void... voids) {
            dao.deleteAll();
            return null;
        }
    }

    // oldest
    private static class OldestAirportsAsyncTask extends AsyncTask<Void, Void, AirportEntity> {
        private AirportDao dao;

        public OldestAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized AirportEntity doInBackground(Void... voids) {
            return dao.getOldest();
        }
    }

    // count
    private static class CountAirportsAsyncTask extends AsyncTask<Void, Void, Integer> {
        private AirportDao dao;

        public CountAirportsAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Integer doInBackground(Void... voids) {
            return dao.getCount();
        }
    }

    // setIsWatched
    private static class SetIsWatchedAsyncTask extends AsyncTask<Integer, Void, Void> {
        private AirportDao dao;

        public SetIsWatchedAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(Integer... ints) {
            dao.setIsWatched(ints[0], 1);
            return null;
        }
    }

    // resetIsWatched
    private static class ResetIsWatchedAsyncTask extends AsyncTask<Integer, Void, Void> {
        private AirportDao dao;

        public ResetIsWatchedAsyncTask(AirportDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(Integer... ints) {
            dao.setIsWatched(ints[0], 0);
            return null;
        }
    }

    /**
     * The following method provide AsyncTask
     * wrapper classes to handle our database
     * queries on background threads
     */


    // Insert

    private static class InsertWatchedAsyncTask extends AsyncTask<List<WatchListEntity>, Void, Void> {
        private static final String TAG = "InsertWatchedAsyncTask";

        private WatchListDao dao;

        public InsertWatchedAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(List<WatchListEntity>... lists) {
            dao.insert(lists[0]);
            return null;
        }
    }

    // Update
    private static class UpdateWatchedAsyncTask extends AsyncTask<WatchListEntity, Void, Void> {
        private WatchListDao dao;

        public UpdateWatchedAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(WatchListEntity... list) {
            dao.update(list[0]);
            return null;
        }
    }

    // delete
    private static class DeleteWatchedAsyncTask extends AsyncTask<WatchListEntity, Void, Void> {
        private WatchListDao dao;

        public DeleteWatchedAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(WatchListEntity... list) {
            dao.delete(list[0]);
            return null;
        }
    }

    private static class DeleteWatchedAirportAsyncTask extends AsyncTask<AirportEntity, Void, Void> {
        private WatchListDao dao;

        public DeleteWatchedAirportAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(AirportEntity... list) {
            dao.deleteWatchedAirport(list[0].getId());
            return null;
        }
    }

    // delete All
    private static class DeleteAllWatchedAsyncTask extends AsyncTask<Void, Void, Void> {
        private WatchListDao dao;

        public DeleteAllWatchedAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Void doInBackground(Void... voids) {
            dao.deleteAll();
            return null;
        }
    }

    // count
    private static class CountWatchedAsyncTask extends AsyncTask<Void, Void, Integer> {
        private WatchListDao dao;

        public CountWatchedAsyncTask(WatchListDao dao) {
            this.dao = dao;
        }

        @Override
        protected synchronized Integer doInBackground(Void... voids) {
            return dao.getCount();
        }
    }

}
