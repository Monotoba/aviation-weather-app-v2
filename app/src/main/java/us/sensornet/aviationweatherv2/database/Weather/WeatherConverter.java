/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/24/18 8:17 PM
 */

package us.sensornet.aviationweatherv2.database.Weather;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;

import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;


public class WeatherConverter {

    @TypeConverter
    public static String fromMetarReport(MetarReport report) {
        Gson gson = new Gson();

        String json = gson.toJson(report);

        return json;
    }

}
