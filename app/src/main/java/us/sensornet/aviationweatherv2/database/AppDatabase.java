/*
 * Copyright (c) Randall Morgan 2018 All rights reserved.
 * Last Modified $file.lastModified.
 * Developed by Randall Morgan on 9/27/18 12:36 PM
 */

package us.sensornet.aviationweatherv2.database;

import android.app.Activity;
import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import us.sensornet.aviationweatherv2.database.Airports.AirportDao;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.Weather.WeatherConverter;
import us.sensornet.aviationweatherv2.database.Weather.metar.MetarObjectDao;
import us.sensornet.aviationweatherv2.database.Weather.metar.MetarObjectEntity;
import us.sensornet.aviationweatherv2.database.converter.CsvToDatabase;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListDao;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListEntity;

@Database(entities = {AirportEntity.class, WatchListEntity.class, MetarObjectEntity.class}, version = 1, exportSchema = false)
@TypeConverters({WeatherConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static final String TAG = "AppDatabase";

    private static volatile AppDatabase INSTANCE;
    private static Context mContext;
    private static IDbImport mListener;
    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            Log.d(TAG, "onCreate: roomCallback called");
            new PopulateDbAsyncTask(INSTANCE).execute();
        }
    };

    public static synchronized AppDatabase getInstance(final Context context, IDbImport listener) {
        Log.d(TAG, "getDatabase: called");
        mContext = context;
        if (null != listener)
            mListener = listener;

        if (INSTANCE == null) {
            Log.d(TAG, "getDatabase: generating room");
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "aviation_weather.db")
                    .addCallback(roomCallback)
                    .build();
        }

        return INSTANCE;
    }

    private static void notifyOnImportStart() {
        if (null != mListener)
            mListener.onImportStart();
    }

    private static void notifyOnImportFinished() {
        if (null != mListener)
            mListener.onImportFinished();
    }

    public abstract AirportDao airportDao();

    public abstract WatchListDao watchListDao();

    public abstract MetarObjectDao metarObjectDao();

    public interface IDbImport {
        void onImportStart();

        void onImportFinished();
    }


    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private static final String TAG = "PopulateDbAsyncTask";

        private AirportDao airportDao;
        private WatchListDao watchListDao;
        private MetarObjectDao metarObjectDao;

        private Activity mNotifier;

        private PopulateDbAsyncTask(AppDatabase db) {
            Log.d(TAG, "PopulateDbAsyncTask: called");
            this.airportDao = db.airportDao();
            this.watchListDao = db.watchListDao();
            this.metarObjectDao = db.metarObjectDao();

        }

        @Override
        protected synchronized void onPreExecute() {
            notifyOnImportStart();
            //super.onPreExecute();
        }

        @Override
        protected synchronized Void doInBackground(Void... voids) {
            new CsvToDatabase((Application) mContext.getApplicationContext(),
                    "world-airports.csv").importFile();
            return null;
        }

        @Override
        protected synchronized void onPostExecute(Void aVoid) {
            notifyOnImportFinished();
            //super.onPostExecute(aVoid);
        }

    }


}
