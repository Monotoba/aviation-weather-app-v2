/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 8:17 PM
 */

package us.sensornet.aviationweatherv2.database.Airports;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface AirportDao {

    @Query("SELECT * FROM airport_table LIMIT 100, :start")
    LiveData<List<AirportEntity>> get(int start);


    @Query("SELECT * FROM airport_table WHERE name  like :stations OR iata_code like :stations OR ident like :stations OR continent  like :stations OR iso_country like :stations OR local_code like :stations OR municipality like :stations OR iso_region like :stations OR latitude_deg like :stations OR longitude_deg like :stations ORDER BY iso_country DESC, name ASC")
    LiveData<List<AirportEntity>> getMatching(String stations);

    @Query("SELECT * FROM airport_table ORDER BY last_retrieved LIMIT 1")
    AirportEntity getOldest();

    @Query("SELECT * FROM airport_table WHERE is_watched = 1 ORDER BY name ASC")
    List<AirportEntity> getWatchedAirports();

    @Query("UPDATE airport_table SET is_watched = :state WHERE id = :id")
    void setIsWatched(int id, int state);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<AirportEntity> airportEntities);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(List<AirportEntity> airportEntities);

    @Query("SELECT count(*) FROM airport_table")
    int getCount();

    @Delete
    void delete(AirportEntity airportEntity);

    @Query("DELETE FROM airport_table")
    void deleteAll();

}
