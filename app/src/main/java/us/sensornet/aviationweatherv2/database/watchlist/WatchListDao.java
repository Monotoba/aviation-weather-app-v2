/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/23/18 3:29 AM
 */

package us.sensornet.aviationweatherv2.database.watchlist;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;

@Dao
public interface WatchListDao {

    @Query("SELECT * FROM watch_list_table")
    LiveData<List<WatchListEntity>> getAll();

    @Query("SELECT * FROM watch_list_table WHERE id = :id")
    LiveData<List<WatchListEntity>> getById(int id);

    @Query("SELECT * FROM watch_list_table WHERE airport_id = :id")
    LiveData<List<WatchListEntity>> getByAirportId(int id);

    @Query("SELECT * FROM watch_list_table WHERE icao = :icao")
    LiveData<List<WatchListEntity>> getWatchedAirportsByICAO(String icao);

    @Query("SELECT * FROM airport_table WHERE id IN (SELECT airport_id FROM watch_list_table) ORDER BY name")
    LiveData<List<AirportEntity>> getWatchedAitrports();

    @Query("SELECT * FROM airport_table WHERE gps_code = :icao")
    LiveData<List<AirportEntity>> getAitrportByIcao(String icao);

    @Query("UPDATE airport_table SET is_watched = :state WHERE id = :id")
    void updateAitrportById(int id, int state);

    @Query("UPDATE airport_table SET is_watched = :state WHERE gps_code = :icao")
    void updateWatchedStateByIcao(String icao, int state);

    @Query("SELECT count(*) FROM watch_list_table")
    int getCount();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<WatchListEntity> entity);

    @Update
    void update(WatchListEntity entity);

    @Delete
    void delete(WatchListEntity entity);

    @Query("DELETE FROM watch_list_table WHERE airport_id = :airport_id")
    void deleteWatchedAirport(int airport_id);

    @Query("DELETE FROM watch_list_table")
    void deleteAll();

}
