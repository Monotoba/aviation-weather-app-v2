/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/23/18 10:38 AM
 */

package us.sensornet.aviationweatherv2.database.Weather.metar;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MetarObjectDao {

    @Query("SELECT * FROM metar_object_table")
    List<MetarObjectEntity> getAll();

    @Query("SELECT * FROM metar_object_table WHERE icao = :icao")
    LiveData<MetarObjectEntity> getByIcao(String icao);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<MetarObjectEntity> entities);

    @Update
    void update(MetarObjectEntity entity);

    @Delete
    void delete(MetarObjectEntity entity);

    @Query("DELETE FROM metar_object_table")
    void deleteAll();


}
