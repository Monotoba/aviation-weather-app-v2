/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 11/1/18 10:42 PM
 */

package us.sensornet.aviationweatherv2.database.converter;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.AppDatabase;

import static java.lang.Integer.parseInt;

public class CsvToDatabase extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "CsvToDatabase";
    private static final Executor mExecutor = Executors.newFixedThreadPool(2);
    private static List<AirportEntity> mAirports = new ArrayList<>();
    private static String mFilename = "";
    private static Context mContext;
    private static AppDatabase mDatabase;
    private static Application mApplication;
    private static ICsvToDatabase mListener = null;

    public CsvToDatabase(Application application, String filename) {
        //Log.d(TAG, "CsvToDatabase: called");
        mApplication = application;
        mContext = application.getApplicationContext();
        mFilename = filename;
        mDatabase = AppDatabase.getInstance(application, null);
        //importFile(); // Moved to doInBackground()
    }

    public static void importFile() {
        //Log.d(TAG, "importFile: importing file: " + mFilename);
        if (null != mListener) {
            mListener.onStartLoading();
        }

        mExecutor.execute(() -> {
            try (
                    BufferedReader reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(mFilename)));

                    CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                            .withFirstRecordAsHeader()
                            .withIgnoreHeaderCase()
                            .withTrim())
            ) {
                int i = 0;
                for (CSVRecord csvRecord : csvParser) {
                    // Accessing values by Header names
                    AirportEntity airport = new AirportEntity();

                    // id
                    airport.setId(parseInt(csvRecord.get("id")));
                    // ident
                    airport.setIdent(csvRecord.get("ident"));
                    // type
                    airport.setType(csvRecord.get("type"));
                    // name
                    airport.setName(csvRecord.get("name"));
                    // latitude_deg
                    airport.setLatitude_deg(csvRecord.get("latitude_deg"));
                    // longitude_deg
                    airport.setLongitude_deg(csvRecord.get("longitude_deg"));
                    // elevation_ft
                    airport.setElevation_ft(csvRecord.get("elevation_ft"));
                    // continent
                    airport.setContinent(csvRecord.get("continent"));
                    // iso_country
                    airport.setIso_country(csvRecord.get("iso_country"));
                    // iso_region
                    airport.setIso_region(csvRecord.get("iso_region"));
                    // municipality
                    airport.setMunicipality(csvRecord.get("municipality"));
                    // scheduled_service
                    airport.setScheduled_service(csvRecord.get("scheduled_service"));
                    // gps_code
                    airport.setGps_code(csvRecord.get("gps_code"));
                    // iata_code
                    airport.setIata_code(csvRecord.get("iata_code"));
                    // local_code
                    airport.setLocal_code(csvRecord.get("local_code"));
                    // home_link
                    airport.setHome_link(csvRecord.get("home_link"));
                    // wikipedia_link
                    airport.setWikipedia_link(csvRecord.get("wikipedia_link"));
                    // keywords	score
                    airport.setScore(csvRecord.get("score"));
                    // last_updated
                    airport.setLast_updated(csvRecord.get("last_updated"));

                    mAirports.add(airport);

                    // Save to list
                    if (i % 100 == 0) {
                        //Log.d(TAG, "importFile: Airports: " + airport.getName());
                        mDatabase.airportDao().insert(mAirports);
                        mAirports.clear();
                    }

                    // Count records
                    i++;
                }

                // Insert any remaining records
                mDatabase.airportDao().insert(mAirports);
                mAirports.clear();
            } catch (Exception e) {
                //Log.d(TAG, "importFile: Error: " + e.getMessage() + ", " + e.getCause());
                if (null != mListener) {
                    mListener.onErrorLoading();
                }
            }

        });

        if (null != mListener) {
            mListener.onStopLoading();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected synchronized Void doInBackground(Void... voids) {
        importFile();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    public void setListener(ICsvToDatabase listener) {
        mListener = listener;
    }

    public interface ICsvToDatabase {
        void onStartLoading();

        void onStopLoading();

        void onErrorLoading();
    }
}
