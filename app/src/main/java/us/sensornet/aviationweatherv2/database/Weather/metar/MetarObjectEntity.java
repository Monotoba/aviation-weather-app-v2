/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/23/18 10:23 AM
 */

package us.sensornet.aviationweatherv2.database.Weather.metar;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "metar_object_table")
public class MetarObjectEntity {

    @PrimaryKey
    @NonNull
    private String icao;
    private String object;
    private String last_retrieved;

    public MetarObjectEntity(@NonNull String icao, String object, String last_retrieved) {
        this.icao = icao;
        this.object = object;
        this.last_retrieved = last_retrieved;
    }

    public String getIcao() {
        return icao;
    }

    public String getObject() {
        return object;
    }

    public String getLast_retrieved() {
        return last_retrieved;
    }

    @Override
    public String toString() {
        return "MetarObjectEntity{" +
                "icao='" + icao + '\'' +
                ", object='" + object + '\'' +
                ", last_retreived='" + last_retrieved + '\'' +
                '}';
    }
}
