/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 8:17 PM
 */

package us.sensornet.aviationweatherv2.database.Airports;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

import java.text.DateFormat;
import java.text.SimpleDateFormat;


@CsvDataType()
@Entity(tableName = "airport_table")
public class AirportEntity {

    @CsvField(pos = 0)
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    int id = 0;

    @CsvField(pos = 1)
    String ident;

    @CsvField(pos = 2)
    String type;

    @CsvField(pos = 3)
    String name;

    @CsvField(pos = 4)
    String latitude_deg;

    @CsvField(pos = 5)
    String longitude_deg;

    @CsvField(pos = 6)
    String elevation_ft;

    @CsvField(pos = 7)
    String continent;

    @CsvField(pos = 8)
    String iso_country;

    @CsvField(pos = 9)
    String iso_region;

    @CsvField(pos = 10)
    String municipality;

    @CsvField(pos = 11)
    String scheduled_service;

    @CsvField(pos = 12)
    String gps_code;

    @CsvField(pos = 13)
    String iata_code;

    @CsvField(pos = 14)
    String local_code;

    @CsvField(pos = 15)
    String home_link;

    @CsvField(pos = 16)
    String wikipedia_link;

    @CsvField(pos = 17)
    String keywords;

    @CsvField(pos = 18)
    String score;

    @CsvField(pos = 19)
    String last_updated;

    String last_retrieved;

    int is_watched;


    @Ignore
    public AirportEntity() {
    }

    //                            ident,        type,        name,        latitude_deg,        longitude_deg,        elevation_ft,        continent,        iso_country,        iso_region,        municipality,        scheduled_service,        gps_code,        iata_code,        local_code,        home_link,        wikipedia_link,        keywords,        score,        last_updated             last_retrieved     is_watched
    public AirportEntity(int id, String ident, String type, String name, String latitude_deg, String longitude_deg, String elevation_ft, String continent, String iso_country, String iso_region, String municipality, String scheduled_service, String gps_code, String iata_code, String local_code, String home_link, String wikipedia_link, String keywords, String score, String last_updated, String last_retrieved, int is_watched) {
        this.id = id;
        this.ident = ident;
        this.type = type;
        this.name = name;
        this.latitude_deg = latitude_deg;
        this.longitude_deg = longitude_deg;
        this.elevation_ft = elevation_ft;
        this.continent = continent;
        this.iso_country = iso_country;
        this.iso_region = iso_region;
        this.municipality = municipality;
        this.scheduled_service = scheduled_service;
        this.gps_code = gps_code;
        this.iata_code = iata_code;
        this.local_code = local_code;
        this.home_link = home_link;
        this.wikipedia_link = wikipedia_link;
        this.keywords = keywords;
        this.score = score;
        this.last_updated = last_updated;
        // Not in original data set from api
        this.last_retrieved = last_retrieved;
        this.is_watched = is_watched;
        // If not set, set the date/time string
        if (last_updated.isEmpty()) {
            DateFormat dFormat = new SimpleDateFormat("dd MM yyyy, HH:mm");
            this.last_updated = dFormat.format(java.util.Calendar.getInstance().getTime());
        }
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude_deg() {
        return latitude_deg;
    }

    public void setLatitude_deg(String latitude_deg) {
        this.latitude_deg = latitude_deg;
    }

    public String getLongitude_deg() {
        return longitude_deg;
    }

    public void setLongitude_deg(String longitude_deg) {
        this.longitude_deg = longitude_deg;
    }

    public String getElevation_ft() {
        return elevation_ft;
    }

    public void setElevation_ft(String elevation_ft) {
        this.elevation_ft = elevation_ft;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getIso_country() {
        return iso_country;
    }

    public void setIso_country(String iso_country) {
        this.iso_country = iso_country;
    }

    public String getIso_region() {
        return iso_region;
    }

    public void setIso_region(String iso_region) {
        this.iso_region = iso_region;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getScheduled_service() {
        return scheduled_service;
    }

    public void setScheduled_service(String scheduled_service) {
        this.scheduled_service = scheduled_service;
    }

    public String getGps_code() {
        return gps_code;
    }

    public void setGps_code(String gps_code) {
        this.gps_code = gps_code;
    }

    public String getIata_code() {
        return iata_code;
    }

    public void setIata_code(String iata_code) {
        this.iata_code = iata_code;
    }

    public String getLocal_code() {
        return local_code;
    }

    public void setLocal_code(String local_code) {
        this.local_code = local_code;
    }

    public String getHome_link() {
        return home_link;
    }

    public void setHome_link(String home_link) {
        this.home_link = home_link;
    }

    public String getWikipedia_link() {
        return wikipedia_link;
    }

    public void setWikipedia_link(String wikipedia_link) {
        this.wikipedia_link = wikipedia_link;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    // Added fields not in original api data
    // below this line
    public String getLast_retrieved() {
        return last_retrieved;
    }

    public void setLast_retrieved(String last_retrieved) {
        this.last_retrieved = last_retrieved;
    }

    public int getIs_watched() {
        return is_watched;
    }

    public void setIs_watched(int is_watched) {
        this.is_watched = is_watched;
    }

    @Override
    public String toString() {
        return "AirportEntity{" +
                "id=" + id +
                ", ident='" + ident + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", latitude_deg='" + latitude_deg + '\'' +
                ", longitude_deg='" + longitude_deg + '\'' +
                ", elevation_ft='" + elevation_ft + '\'' +
                ", continent='" + continent + '\'' +
                ", iso_country='" + iso_country + '\'' +
                ", iso_region='" + iso_region + '\'' +
                ", municipality='" + municipality + '\'' +
                ", scheduled_service='" + scheduled_service + '\'' +
                ", gps_code='" + gps_code + '\'' +
                ", iata_code='" + iata_code + '\'' +
                ", local_code='" + local_code + '\'' +
                ", home_link='" + home_link + '\'' +
                ", wikipedia_link='" + wikipedia_link + '\'' +
                ", keywords='" + keywords + '\'' +
                ", score='" + score + '\'' +
                ", last_updated='" + last_updated + '\'' +
                ", last_retrieved='" + last_retrieved + '\'' +
                ", is_watched=" + is_watched +
                '}';
    }

    public static final class FIELDS {
        public static final int ID = 0;
        public static final int IDENT = 1;
        public static final int TYPE = 2;
        public static final int NAME = 3;
        public static final int LATITUDE_DEG = 4;
        public static final int LONGITUDE_DEG = 5;
        public static final int ELEVATION_FT = 6;
        public static final int CONTINENT = 7;
        public static final int ISO_COUNTRY = 8;
        public static final int ISO_REGION = 9;
        public static final int MUNICIPALITY = 10;
        public static final int SCHEDULE_SERVICE = 11;
        public static final int GPS_CODE = 12;
        public static final int IATA_CODE = 13;
        public static final int LOCAL_CODE = 14;
        public static final int HOME_LINK = 15;
        public static final int WIKIPEDIA_LINK = 16;
        public static final int KEYWORDS = 17;
        public static final int SCORE = 18;
        public static final int LAST_UPDATED = 19;
        public static final int LAST_RETRIEVED = 20;
        public static final int IS_WATCHED = 21;
        public static final String TIME_FORMAT_STRING = " YYYY-MM-DDTHH:MM:SSZ ";

    }
}
