/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/23/18 3:29 AM
 */

package us.sensornet.aviationweatherv2.database.watchlist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;

@Entity(tableName = "watch_list_table", foreignKeys = {@ForeignKey(entity = AirportEntity.class,
        parentColumns = {"id"},
        childColumns = {"airport_id"})},
        indices = {@Index(value = {"airport_id"},
                unique = true)})
public class WatchListEntity {

    @PrimaryKey(autoGenerate = true)
    int id;
    int airport_id;
    String icao;

    @Ignore
    public WatchListEntity() {
        // Empty constructor
    }

    public WatchListEntity(int id, int airport_id, String icao) {
        this.id = id;
        this.airport_id = airport_id;
        this.icao = icao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAirport_id() {
        return airport_id;
    }

    public void setAirport_id(int airport_id) {
        this.airport_id = airport_id;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    @Override
    public String toString() {
        return "WatchListEntity{" +
                "id=" + id +
                ", airport_id=" + airport_id +
                ", icao='" + icao + '\'' +
                '}';
    }
}
