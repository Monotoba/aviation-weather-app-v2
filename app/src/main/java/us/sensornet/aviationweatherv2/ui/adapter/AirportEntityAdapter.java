/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/21/18 6:47 PM
 */

package us.sensornet.aviationweatherv2.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;


/**
 * {@link RecyclerView.Adapter} that can display a {@link AirportEntity} and makes a call to the
 */
public class AirportEntityAdapter extends RecyclerView.Adapter<AirportEntityAdapter.ViewHolder> {

    private static final String TAG = "AirportListAdapter";

    private List<AirportEntity> mAirportList = new ArrayList<>();

    private Context mContext;

    private Activity parentActivity;

    private OnItemClickListener mListener;

    public AirportEntityAdapter(Activity activity) {
        parentActivity = activity;
    }

    public void setData(List<AirportEntity> mAirportList) {
        Log.d(TAG, "setData: called");
        this.mAirportList = new ArrayList<>();
        this.mAirportList.addAll(mAirportList);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {
        AirportEntity airport = mAirportList.get(pos);
        viewHolder.tvAirportName.setText(airport.getName().replace("\"", "").trim());
        viewHolder.tvAirportCode.setText(airport.getIdent());

        // Set the switch state
        if (null == airport.getIdent() || airport.getIdent().isEmpty()) {
            viewHolder.swTracking.setVisibility(View.GONE);
        } else {
            int watchedState = airport.getIs_watched();
            if (0 == watchedState) {
                viewHolder.swTracking.setChecked(false);
            } else {
                viewHolder.swTracking.setChecked(true);
            }
        }

        // Don't layout the location if the value is empty.
        if (!mAirportList.get(pos).getMunicipality().isEmpty()) {
            viewHolder.tvLocation.setText(mAirportList.get(pos).getMunicipality());
        } else {
            viewHolder.tvLocation.setVisibility(View.GONE);
        }

        viewHolder.tvCountry.setText(mAirportList.get(pos).getIso_country());


    }

    @NonNull
    @Override
    public AirportEntityAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        // create a new view
        ViewGroup itemView = (ViewGroup) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_airport_entity, viewGroup, false);

        ViewHolder holder = new ViewHolder(itemView);

        return holder;
    }


    @Override
    public int getItemCount() {
        if (null == mAirportList)
            return 0;

        return mAirportList.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(AirportEntity airport);

        void onSwitchClickOn(AirportEntity airport);

        void onSwitchClickOff(AirportEntity airport);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAirportName;
        TextView tvAirportCode;
        TextView tvLocation;
        Switch swTracking;
        TextView tvCountry;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvAirportName = itemView.findViewById(R.id.tv_airport_name);
            tvAirportCode = itemView.findViewById(R.id.tv_airport_code);
            tvLocation = itemView.findViewById(R.id.tv_location);
            tvCountry = itemView.findViewById(R.id.tv_country);
            swTracking = itemView.findViewById(R.id.sw_tracking_switch);

            swTracking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Switch switchView = (Switch) view;
                    int position = getAdapterPosition();

                    if (null != mListener && position != RecyclerView.NO_POSITION) {

                        if (switchView.isChecked()) {
                            mListener.onSwitchClickOn(mAirportList.get(position));
                        } else {
                            mListener.onSwitchClickOff(mAirportList.get(position));
                        }
                    }

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (null != mListener && position != RecyclerView.NO_POSITION) {
                        mListener.onItemClick(mAirportList.get(position));
                    }
                }
            });
        }
    }

}

