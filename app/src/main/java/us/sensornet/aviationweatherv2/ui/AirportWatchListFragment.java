/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 4:33 PM
 */

package us.sensornet.aviationweatherv2.ui;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListEntity;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.network.ResponseEnvelope;
import us.sensornet.aviationweatherv2.service.AppWidgetService;
import us.sensornet.aviationweatherv2.ui.adapter.WatchListAdapter;
import us.sensornet.aviationweatherv2.viewmodel.AirportViewModel;
import us.sensornet.aviationweatherv2.viewmodel.WatchListFragmentViewModel;

/**
 * A fragment representing a list of Items.
 */
public class AirportWatchListFragment extends Fragment {
    public static final String KEY_POSITION_ING = "AirportWatchListFragment.recycler.scroll_position";
    private static final String TAG = "AirportWatchListFragmen";
    private static final String BUNDLE_RECYCLER_LAYOUT = "AirportWatchListFragment.recycler.layout";
    public static int mPositionIng;

    private AirportViewModel mViewModel;
    private List<AirportEntity> mAirports = new ArrayList<>();

    private WatchListFragmentViewModel mWatchViewModel;
    private List<WatchListEntity> mWatchList = new ArrayList<>();
    private List<MetarReport> mMetarReports = new ArrayList<>();


    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private WatchListAdapter mAdapter;
    private Bundle mRecyclerViewLayoutState;

    private String mIcaoString;

    private ProgressBar pbLoading;

    private int mLoadCounter = 0;

    private AdView mAdView;

    public static final String EXTRA_DATA = "us.sensornet.aviationweatherv2.EXTRA_DATA";


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AirportWatchListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d(TAG, "onCreate: called");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Log.d(TAG, "onCreateView: called");
        if (null != container.getParent()) {
            container.removeAllViews();
        }
        View view = inflater.inflate(R.layout.fragment_watched_airports_list, container, false);

        pbLoading = view.findViewById(R.id.pb_loading);

        mAdapter = new WatchListAdapter(getContext());
        mRecyclerView = view.findViewById(R.id.rcv_watch_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        if (null == mRecyclerView)
            Log.d(TAG, "onCreateView: mRecyclerView is null");

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mWatchViewModel = ViewModelProviders.of(this).get(WatchListFragmentViewModel.class);
        getWatchedAirports();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");
        mLoadCounter = 0;
        if (null != mRecyclerViewLayoutState) {
            if (null != mRecyclerViewLayoutState) {
                mRecyclerView.getLayoutManager().onRestoreInstanceState(mRecyclerViewLayoutState);
            }
        }
    }


    private void getWatchedAirports() {
        mWatchViewModel.getWatchedAirports().observe(this, new android.arch.lifecycle.Observer<List<AirportEntity>>() {
            @Override
            public void onChanged(@Nullable List<AirportEntity> airportEntities) {
                mAirports = airportEntities;
                Log.d(TAG, "getWatchedAirports-onChanged: called, received " + String.valueOf(mAirports.size()) + " items");

                if (mAirports.size() > 0) {
                    String icao_list = "";
                    Log.d(TAG, "onChanged: mAirports.Size > 0");

                    for (AirportEntity airport : mAirports) {
                        if (null == airport.getIdent() || airport.getIdent().trim().isEmpty())
                            continue;
                        icao_list = icao_list + airport.getIdent() + ",";
                    }

                    icao_list.replace(",,", ",");

                    if (icao_list.length() > 1) {
                        icao_list = icao_list.substring(0, icao_list.length() - 1);
                    }

                    mIcaoString = icao_list;
                    getMetarReports();
                } else {
                    Log.d(TAG, "onChanged: mAirpports.Size = 0");
                }

            }
        });
    }


    private void getMetarReports() {
        Log.d(TAG, "getMetarReports: called");
        mWatchViewModel.getMetarReport(mIcaoString).observe(this, new Observer<ResponseEnvelope<List<MetarReport>>>() {
            @Override
            public void onChanged(@Nullable ResponseEnvelope<List<MetarReport>> envelope) {
                Log.d(TAG, "getMetarReports-onChanged: called for getMetarReport()");
                if (null != mWatchViewModel.mMetarReports.getValue()) {
                    if (mWatchViewModel.mMetarReports.getValue().getIsError()) {
                        Toast.makeText(getActivity(), "Cannot load METAR reports", Toast.LENGTH_SHORT).show();
                    } else {
                        mMetarReports = mWatchViewModel.mMetarReports.getValue().getData();

                        if (mMetarReports.isEmpty()) {
                            Log.d(TAG, "onChanged: mMetarReports is Empty");
                            // TODO Done - Move text to string resource
                            Toast.makeText(getContext(), getString(R.string.no_data_available), Toast.LENGTH_LONG).show();
                        } else {

                            if (null != mMetarReports) {
                                Log.d(TAG, "getMetarReports-onChanged : mMetarReports not null");
                                mAdapter.setData(mMetarReports);
                                mAdapter.notifyDataSetChanged();

                                updateAppWidget(mMetarReports);

                            } else {
                                Log.d(TAG, "getMetarReports-onChanged: mMetarReports is null");
                            }
                        }

                    }

                } else {
                    Log.d(TAG, "onChanged: MetarReport Envelope is null");
                    Toast.makeText(getActivity(), "Can not load METAR Reports", Toast.LENGTH_SHORT).show();
                }

                showLoading(false);
            }
        });
    }


    private void updateAppWidget(List<MetarReport> reports) {
        Log.d(TAG, "updateAppWidget: calling appWidget.notifyAppWidgetDataChanged()");
        Context context = getActivity().getApplicationContext();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, AppWidgetProvider.class));

        Gson gson = new Gson();
        String jsonData = gson.toJson(mMetarReports.toArray());

        Bundle dataBundle = new Bundle();
        dataBundle.putString(EXTRA_DATA, jsonData);

        Intent updateIntent = new Intent(getActivity(),AppWidgetService.class);
        updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        updateIntent.putExtra(EXTRA_DATA, dataBundle);

        appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, AppWidgetProvider.class));
        context.sendBroadcast(updateIntent);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.app_widget_stack_view);

    }

    private void showLoading(boolean show) {
        if (null == pbLoading) {
            pbLoading = getActivity().findViewById(R.id.pb_loading);
        }
        if (show) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (mPositionIng != RecyclerView.NO_POSITION) {

            mRecyclerView.getLayoutManager().scrollToPosition(mPositionIng);
        }

        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        //super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());

        int scrollPositionIng = mRecyclerView.computeVerticalScrollOffset();

        mPositionIng = scrollPositionIng;

        outState.putInt(KEY_POSITION_ING, mPositionIng);
        super.onSaveInstanceState(outState);
    }
}
