/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/27/18 5:46 PM
 */

package us.sensornet.aviationweatherv2.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.Weather.metar.MetarObjectEntity;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Cloud;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.Condition;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.utils.FlightRulesToColor;
import us.sensornet.aviationweatherv2.viewmodel.WatchListViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link WatchItemDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WatchItemDetailFragment extends Fragment {
    private static final String TAG = "WatchItemDetailFragment";

    int mPosition;
    String mIcao;

    WatchListViewModel mViewModel;
    MetarReport mMetarReport;

    Gson mGson = new Gson();

    FlightRulesToColor mFlightRulesColor;
    char DegreeSym = +(char) 0x00B0;
    TextView tvStationName;
    TextView tvICAOCode;
    TextView tvFlightRules;
    TextView tvObserved;
    TextView tvConditions;
    TextView tvPressure;
    TextView tvCeiling;
    TextView tvClouds;
    TextView tvWind;
    TextView tvDewpoint;
    TextView tvHumidity;
    TextView tvRain;
    TextView tvSnow;
    TextView tvTemperature;
    TextView tvVisibility;
    TextView tvElevation;
    TextView tvRawMetar;
    private ProgressBar pbLoading;

    public WatchItemDetailFragment() {
        // Required empty public constructor

    }


    public static WatchItemDetailFragment newInstance() {
        WatchItemDetailFragment fragment = new WatchItemDetailFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFlightRulesColor = new FlightRulesToColor(getActivity().getApplication());

        if (getArguments() != null) {
            // Get the passed arguments
            Bundle bundle = getArguments();
            mPosition = bundle.getInt("POSITION");
            mIcao = bundle.getString("ICAO");
            //Log.d(TAG, "onCreateView: recieved ICAO: " + mIcao + " position: " + String.valueOf(mPosition));
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watch_item_detail, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        pbLoading = view.findViewById(R.id.pb_loading);

        tvStationName = view.findViewById(R.id.tv_value_station_name);
        tvICAOCode = view.findViewById(R.id.tv_value_icao);
        tvFlightRules = view.findViewById(R.id.tv_value_flight_rules);
        tvObserved = view.findViewById(R.id.tv_value_observed);
        tvConditions = view.findViewById(R.id.tv_value_conditions);
        tvPressure = view.findViewById(R.id.tv_value_pressure);
        tvCeiling = view.findViewById(R.id.tv_value_ceiling);
        tvClouds = view.findViewById(R.id.tv_value_clouds);
        tvWind = view.findViewById(R.id.tv_value_wind);
        tvDewpoint = view.findViewById(R.id.tv_value_dewpoint);
        tvHumidity = view.findViewById(R.id.tv_value_humidity);
        tvRain = view.findViewById(R.id.tv_value_rain);
        tvSnow = view.findViewById(R.id.tv_value_snow);
        tvTemperature = view.findViewById(R.id.tv_value_temperature);
        tvVisibility = view.findViewById(R.id.tv_value_visibility);
        tvElevation = view.findViewById(R.id.tv_value_elevation);
        tvRawMetar = view.findViewById(R.id.tv_value_raw_metar);

        showLoading(false);

        // Get ViewModel
        mViewModel = ViewModelProviders.of(this).get(WatchListViewModel.class);

        mViewModel.getCurrentStationReport(mIcao).observe(this, new Observer<MetarObjectEntity>() {
                    @Override
                    public void onChanged(@Nullable MetarObjectEntity metarObjectEntity) {
                        mMetarReport = mGson.fromJson(metarObjectEntity.getObject(), MetarReport.class);

                        if (null != mMetarReport) {
                            Log.d(TAG, "onChanged: mMetarReport: " + mMetarReport.toString());

                            // Fill in the display info
                            prepareStation(tvStationName);

                            prepareIcaoCode(tvICAOCode);

                            prepareFlightRules(tvFlightRules);

                            prepareObservedTime(tvObserved);

                            preparedConditions(tvConditions);

                            prepareBarometer(tvPressure);

                            prepareCeiling(tvCeiling);

                            prepareClouds(tvClouds);

                            prepareWind(tvWind);

                            prepareDewpoint(tvDewpoint);

                            prepareHumidity(tvHumidity);

                            prepareRain(tvRain);

                            prepareSnow(tvSnow);

                            prepareTemperature(tvTemperature);

                            prepareVisibility(tvVisibility);

                            prepareElevation(tvElevation);

                            prepareRawMetar(tvRawMetar);
                        }

                        showLoading(false);
                    }

                }
        );

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's state here
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
        }
    }


    private void prepareStation(TextView view) {
        view.setText(mMetarReport.getStation().getName());
    }


    private void prepareIcaoCode(TextView view) {
        view.setText(mMetarReport.getIcao());
    }


    private void prepareFlightRules(TextView view) {
        if (null != mMetarReport.getFlightCategory()) {
            view.setText(mMetarReport.getFlightCategory());
            // Set background color for Flight Rules
            FlightRulesToColor colorHandler = new FlightRulesToColor(getActivity().getApplication());
            view.setBackgroundColor(mFlightRulesColor.getColor(mMetarReport.getFlightCategory()));
        } else {
            // TODO DONE - move text to resource
            view.setText(R.string.unknown_text);
            view.setBackgroundColor(FlightRulesToColor.getColor("unknown"));
        }
    }


    private void prepareObservedTime(TextView view) {
        if (null != mMetarReport.getObserved()) {
            view.setText(mMetarReport.getObserved());
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_observed).setVisibility(View.GONE);
        }
    }


    private void preparedConditions(TextView view) {
        String sConditions = "";
        if (null != mMetarReport.getConditions()) {

            for (Condition cond : mMetarReport.getConditions()) {
                sConditions.concat(cond.getText() + ", ");
            }
            if (sConditions.length() > 2)
                sConditions = sConditions.substring(0, sConditions.length() - 2);
            Log.d(TAG, "preparedConditions: Conditions: " + sConditions);
            if(!sConditions.trim().equals("")) {
                view.setText(sConditions);
                Log.d(TAG, "preparedConditions: sConditions: " + sConditions);
            } else {
                view.setVisibility(View.GONE);
                getActivity().findViewById(R.id.tv_label_conditions).setVisibility(View.GONE);
            }
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_conditions).setVisibility(View.GONE);
        }
    }


    private void prepareBarometer(TextView view) {
        if (null != mMetarReport.getBarometer()) {
            view.setText(String.valueOf(mMetarReport.getBarometer().getKpa()));
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_pressure).setVisibility(View.GONE);
        }
    }


    private void prepareCeiling(TextView view) {
        if (null != mMetarReport.getCeiling()) {
            String msg = mMetarReport.getCeiling().getText() + getString(R.string.space_at_space) + mMetarReport.getCeiling().getFeetAgl() + getString(R.string.space_feet_agl_space);
            view.setText(msg);
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_ceiling).setVisibility(View.GONE);
        }
    }


    private void prepareClouds(TextView view) {
        String cloudDesc = "";
        if (null != mMetarReport.getClouds()) {
            for (Cloud cloud : mMetarReport.getClouds()) {
                cloudDesc += cloud.getCode() + getString(R.string.space_at_space)
                        + cloud.getBaseFeetAgl() + getString(R.string.comma_space);
            }
            cloudDesc = cloudDesc.substring(0, cloudDesc.length() - 2);
            view.setText(cloudDesc);
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_clouds).setVisibility(View.GONE);
        }
    }

    private void prepareWind(TextView view) {
        if (null != mMetarReport.getWind()) {
            String windDesc = "";
            if(null != mMetarReport.getWind().getSpeedMph() && !mMetarReport.getWind().getSpeedMph().toString().isEmpty()) {
                windDesc = getString(R.string.at)
                        + " "
                        + mMetarReport.getWind().getSpeedMph()
                        + getString((R.string.mph));
            }

            if(null != mMetarReport.getWind().getDegrees() && !mMetarReport.getWind().getDegrees().toString().isEmpty()) {
                windDesc += getString(R.string.space)
                        + getString(R.string.from)
                        + " "
                        + mMetarReport.getWind().getDegrees() + DegreeSym;
            }

            if (null != mMetarReport.getWind().getGustMph()) {
                windDesc += getString(R.string.space)
                        + getString(R.string.gusting_to)
                        + getString(R.string.space)
                        + mMetarReport.getWind().getGustMph()
                        + getString(R.string.space)
                        + getString(R.string.mph);
            }

            view.setText(windDesc);

        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_wind).setVisibility(View.GONE);
        }
    }


    private void prepareDewpoint(TextView view) {
        if (null != mMetarReport.getDewpoint()) {
            String dewpointDesc;
            dewpointDesc = mMetarReport.getDewpoint().getFahrenheit()
                    + getString(R.string.fahrenheit);
            view.setText(dewpointDesc);
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_dewpoint).setVisibility(View.GONE);
        }
    }


    private void prepareHumidity(TextView view) {
        if (null != mMetarReport.getHumidityPercent() && mMetarReport.getHumidityPercent().toString().isEmpty()) {
            // TODO Done - use string resource
            String humidityDesc;
            humidityDesc = mMetarReport.getHumidityPercent()
                    + getString(R.string.percent_sign);
            view.setText(humidityDesc);
            getActivity().findViewById(R.id.tv_label_humidity).setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_humidity).setVisibility(View.GONE);
        }
    }


    private void prepareRain(TextView view) {
        if (null != mMetarReport.getRainIn()) {
            view.setText(String.valueOf(mMetarReport.getRainIn()));
        } else {
            getActivity().findViewById(R.id.tv_label_rain).setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }


    }

    private void prepareSnow(TextView view) {
        if (null != mMetarReport.getSnowIn()) {
            view.setText(String.valueOf(mMetarReport.getSnowIn()));
        } else {
            getActivity().findViewById(R.id.tv_label_snow).setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }
    }


    private void prepareTemperature(TextView view) {
        if (null != mMetarReport.getTemperature()) {
            String msg = mMetarReport.getTemperature().getFahrenheit().toString() + getString(R.string.fahrenheit);
            view.setText(msg);
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_temperature).setVisibility(View.GONE);
        }
    }


    private void prepareVisibility(TextView view) {
        if (null != mMetarReport.getVisibility()) {
            if ((mMetarReport.getVisibility().getMiles() != null)
                    || (mMetarReport.getVisibility().getMiles().trim().isEmpty())) {
                String msg = mMetarReport.getVisibility().getMiles() + getString(R.string.space_miles);
                view.setText(msg);
            }
        } else {
            view.setVisibility(View.GONE);
            getActivity().findViewById(R.id.tv_label_visibility).setVisibility(View.GONE);
        }
    }


    private void prepareElevation(TextView view) {
        String msg = mMetarReport.getElevation().getFeet().toString() + getString(R.string.space_feet);
        view.setText(msg);
    }


    private void prepareRawMetar(TextView view) {
        view.setText(mMetarReport.getRawText());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void showLoading(boolean show) {
        if (null == pbLoading) {
            pbLoading = getActivity().findViewById(R.id.pb_loading);
        }
        if (show) {
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            pbLoading.setVisibility(View.GONE);
        }
    }

}
