/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/30/18 10:48 PM
 */

package us.sensornet.aviationweatherv2.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import us.sensornet.aviationweatherv2.R;

public class DirectionalIndicator extends View {

    private static final int SQUARE_SIZE = 20;

    private Rect mRect;

    private Paint mPaint;

    private Paint mPaintInner;

    private int mColor;

    private int mBackground;

    private float mRotateDegrees;

    private boolean mHasBorder;

    private int mBorderStroke;

    private int mBorderColor;

    private Bitmap mBitmap;

    private Bitmap mBitmapResized;

    private Bitmap mBitmapRotated;

    private float cx, cy;

    public DirectionalIndicator(Context context) {
        super(context);
        init(null);
    }

    public DirectionalIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public DirectionalIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DirectionalIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public static Bitmap getRotatedBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, source.getWidth() / 2, source.getHeight() / 2);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void init(@Nullable AttributeSet set) {
        mRect = new Rect();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintInner = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHasBorder = false;

        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.wind_indicator_00_100, null);

        TypedArray ta = getContext().obtainStyledAttributes(set, R.styleable.DirectionalIndicator);

        mColor = ta.getColor(R.styleable.DirectionalIndicator_color, Color.BLUE);
        //mBackground = ta.getColor(R.styleable.DirectionalIndicator_background_color, Color.TRANSPARENT);
        mBorderColor = ta.getColor(R.styleable.DirectionalIndicator_border_color, Color.BLUE);
        mBorderStroke = ta.getInt(R.styleable.DirectionalIndicator_border_width, 1);
        mRotateDegrees = ta.getFloat(R.styleable.DirectionalIndicator_heading, 0f);

        ta.recycle();

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mBitmapResized = getResizedBitmap(mBitmap, getWidth(), getHeight());
                mBitmapRotated = getRotatedBitmap(mBitmapResized, mRotateDegrees);
            }
        });
        if (set == null)
            return;


    }

    public void setColor(String color) {

    }

    public void setRotation(int degrees) {
        mRotateDegrees = degrees;
    }

    public void setBorder(int width) {
        if (width == 0)
            mHasBorder = false;


    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(mColor);
        mPaintInner.setColor(mBackground);
        mPaintInner.setAntiAlias(true);

        float radius = 10f;
        float radiusInner = 8f;
        cx = getWidth() / 2;
        cy = getHeight() / 2;
        radius = (int) (getWidth() * 0.8) / 2;
        radiusInner = (int) (getWidth() * 0.7) / 2;

        canvas.drawCircle(cx, cy, radius, mPaint);
        canvas.drawCircle(cx, cy, radiusInner, mPaintInner);

        // Move the center of the bitmap to
        // the center of the canvas
        // note: must call canvas.getWidth/getHeight
        // for this to work.
        if (null != mBitmapRotated) {
            cx = (canvas.getWidth() - mBitmapRotated.getWidth()) / 2.0f;
            cy = (canvas.getHeight() - mBitmapRotated.getHeight()) / 2.0f;
            canvas.drawBitmap(mBitmapRotated, cx, cy, null);
        }

        super.onDraw(canvas);
    }

    private Bitmap getResizedBitmap(Bitmap bitmap, int reqWidth, int reqHeight) {
        Matrix matrix = new Matrix();

        RectF src = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF dest = new RectF(0, 0, reqWidth, reqHeight);
        matrix.setRectToRect(src, dest, Matrix.ScaleToFit.FILL);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
}
