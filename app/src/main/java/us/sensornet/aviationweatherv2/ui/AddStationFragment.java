/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/21/18 6:48 PM
 */

package us.sensornet.aviationweatherv2.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import java.util.List;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.watchlist.WatchListEntity;
import us.sensornet.aviationweatherv2.ui.adapter.AirportEntityAdapter;
import us.sensornet.aviationweatherv2.viewmodel.AirportViewModel;
import us.sensornet.aviationweatherv2.viewmodel.WatchListViewModel;


/**
 * TODO Done - Ths fragment lists all the airports
 * in the database and provides a filter method
 * to aid in locating the airports (stations)
 * the user wishes to follow (watch).
 * <p>
 * The user may select stations in the list to
 * to add to their watch list. Or, onselect
 * stations they are watcing to remove them
 * from their watch list.
 */
public class AddStationFragment extends Fragment {
    private static final String TAG = "AddStationFragment";

    private static final String BUNDLE_RECYCLER_LAYOUT = "AddStationFragment.recycler.layout";

    private Bundle savedRecyclerLayoutState;

    private Integer mScrollPosition;

    private AirportViewModel mViewModel;

    private List<AirportEntity> mAirports;

    private RecyclerView mRecyclerView;

    private AirportEntityAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;

    private ProgressBar pbLoading;


    public AddStationFragment() {
        // Required empty public constructor

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showLoading(true);

        mAdapter = new AirportEntityAdapter(getActivity());
        mRecyclerView = getActivity().findViewById(R.id.rcv_airport_list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        Log.d(TAG, "AddStationFragment onActivityCreated: called");
        mViewModel = ViewModelProviders.of(getActivity()).get(AirportViewModel.class);

        mViewModel.mAirports.observe(this, new android.arch.lifecycle.Observer<List<AirportEntity>>() {
            @Override
            public void onChanged(@Nullable List<AirportEntity> airportEntities) {
                Log.d(TAG, "AddStationFragment onChanged: called");
                mAirports = airportEntities;
                Log.d(TAG, "AddStationFragment onChanged: called, received " + String.valueOf(mAirports.size()) + " items");
                mAdapter.setData(airportEntities);
                if (null != savedRecyclerLayoutState) {
                    mLayoutManager.onRestoreInstanceState(savedRecyclerLayoutState);
                    // set to null so we don't call this on later onChange method calls
                    savedRecyclerLayoutState = null;
                }

                showLoading(false);
            }
        });

        ImageButton filterButton = getActivity().findViewById(R.id.img_btn_filter_station_list);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "AddStationFragment onClick: called in fragment");

                switch (v.getId()) {
                    case R.id.img_btn_filter_station_list:
                        showLoading(true);
                        View parent = (View) v.getParent();
                        EditText edit = parent.findViewById(R.id.editText);
                        //Log.d(TAG, "onClick: called with edit text of: " + edit.getText().toString());
                        // If search string is empty set wildcard search
                        String query = !edit.getText().toString().isEmpty() ?
                                edit.getText().toString() : "%";
                        mViewModel.setFilter(query);
                        mViewModel.getAirports();
                        showLoading(false);
                        return;
                    default:

                }

            }
        });

        //Log.d(TAG, "AddStationFragment onActivityCreated: call2");

        // TODO Done - Add List Item Click Handle (See Interface in Adapter)
        mAdapter.setOnItemClickListener(new AirportEntityAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(AirportEntity airport) {
                // Do nothing here
            }

            // TODO Done - Get ICAO/IDENT codes into watched table
            @Override
            public void onSwitchClickOn(AirportEntity airport) {
                WatchListEntity watched = new WatchListEntity();
                watched.setAirport_id(airport.getId());
                watched.setIcao(airport.getIdent());
                WatchListViewModel viewModel = new WatchListViewModel(getActivity().getApplication());
                viewModel.insertWatchedAirport(airport);
            }

            @Override
            public void onSwitchClickOff(AirportEntity airport) {
                WatchListEntity watched = new WatchListEntity();
                watched.setAirport_id(airport.getId());
                WatchListViewModel viewModel = new WatchListViewModel(getActivity().getApplication());
                viewModel.deleteWatchedAirport(airport);
            }
        });

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Log.d(TAG, "AddStationFragment onCreateView: called");
        return inflater.inflate(R.layout.fragment_add_station, container, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //Log.d(TAG, "AddStationFragment onSaveInstanceState: called. Saving recycler view position");
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, mRecyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        //Log.d(TAG, "AddStationFragment onViewStateRestored: called");
        if (null != savedInstanceState) {
            Log.d(TAG, "AddStationFragment onViewStateRestored: Restoring RecyclerView position");
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
            mRecyclerView.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //Log.d(TAG, "AddStationFragment onResume: called");
        //showLoading(true);
        mViewModel.getAirports();
        //showLoading(false);
    }

    private void showLoading(boolean show) {
        Log.d(TAG, "AddStationFragment showLoading: called");
        if (null == pbLoading) {
            pbLoading = getActivity().findViewById(R.id.pb_loading);
            Log.d(TAG, "AddStationFragment showLoading: called");
        }
        if (show) {
            Log.d(TAG, "AddStationFragment showLoading: called on");
            pbLoading.setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG, "AddStationFragment showLoading: called off");
            pbLoading.setVisibility(View.INVISIBLE);
            pbLoading.setVisibility(View.GONE);
        }
    }
}
