/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/21/18 6:07 PM
 */

package us.sensornet.aviationweatherv2.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.AppDatabase;
import us.sensornet.aviationweatherv2.service.SyncSchedular;
import us.sensornet.aviationweatherv2.viewmodel.WatchListViewModel;


public class MainActivity extends AppCompatActivity implements AppDatabase.IDbImport {
    private static final String TAG = "MainActivity";

    private FirebaseAnalytics mFirebaseAnalytics;


    private WatchListViewModel mViewModel;

    private Fragment mWatchList;

    private Fragment mAddStation;

    private AdView mAdView;

    private ProgressBar pbMainLoading;

    // Used for backstack management.
    private boolean firstLoaded = false; // true once the first fragment is loaded.

    // For jobschedular
    private String JOB_TAG = "SYNC_SERVICE";
    private final int ALARM_INTERVAL_SECONDS = 120;//900;
    private final int SYNC_FLEX_TIME_SECONDS = 30; //300;  // 60 seconds * 5 minutes

    // Transitions
    private static final long MOVE_DEFAULT_TIME = 1000;
    private static final long FADE_DEFAULT_TIME = 300;

    private FragmentManager mFragmentManager;

    private Handler mDelayedTransactionHandler = new Handler();
    private Runnable mRunnable = this::performTransition;


    private void performTransition() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Firebase crashlytics Debug
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)  // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);

        // Create a new dispatcher using the Google Play driver.
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));

        Job syncJob = dispatcher.newJobBuilder()
                .setService(SyncSchedular.class) // the JobService that will be called
                .setTag(JOB_TAG)        // uniquely identifies the job
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setRecurring(true)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setTrigger(Trigger.executionWindow(ALARM_INTERVAL_SECONDS, ALARM_INTERVAL_SECONDS + SYNC_FLEX_TIME_SECONDS))
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .build();

        dispatcher.mustSchedule(syncJob);


        // Get & hide progress bar
        pbMainLoading = findViewById(R.id.pb_main_loading);
        pbMainLoading.setVisibility(View.INVISIBLE);
        // Cause loading of initial data if this
        // is the first time the app has ran
        AppDatabase.getInstance(getApplication(), this);

        // AdMob
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        if (savedInstanceState != null) {
            //Restore the fragment's instance
            mWatchList = getSupportFragmentManager().getFragment(savedInstanceState, "AirportWatchListFragment");
            mAddStation = getSupportFragmentManager().getFragment(savedInstanceState, "AddStationFragment");
        }

        FloatingActionButton btnAddStation = findViewById(R.id.fab_action_add_station);
        btnAddStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewGroup container = findViewById(R.id.fragment_main_container);
                if (null != container) {
                    container.removeAllViews();
                }
                showAddStationFragment();
            }
        });


        mViewModel = ViewModelProviders.of(this).get(WatchListViewModel.class);
        mViewModel.getWatchedAirports().observe(this, new Observer<List<AirportEntity>>() {
            @Override
            public void onChanged(@Nullable List<AirportEntity> airportEntities) {

                if (airportEntities.size() > 0) {
                    showWacthListFragment();
                } else {
                    // Add the place holder view
                    showNoStationsSelected();
                }
            }
        });

        // Fragment transition

    }


    private void setupSyncService(String icao_list) {
        // Create a new dispatcher using the Google Play driver.
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));

        Bundle IcaoBundle = new Bundle();

        IcaoBundle.putString("ICAO_LIST", icao_list);

        Job syncJob = dispatcher.newJobBuilder()
                .setService(SyncSchedular.class) // the JobService that will be called
                .setTag(JOB_TAG)        // uniquely identifies the job
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setRecurring(true)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setTrigger(Trigger.executionWindow(ALARM_INTERVAL_SECONDS, ALARM_INTERVAL_SECONDS + SYNC_FLEX_TIME_SECONDS))
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setExtras(IcaoBundle)
                .build();

        dispatcher.mustSchedule(syncJob);
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        //Save the fragment's instance
        getSupportFragmentManager().putFragment(outState, "AirportWatchListFragment", mWatchList);
        getSupportFragmentManager().putFragment(outState, "AddStationFragment", mAddStation);

    }

    /*

    FragmentTransaction transaction = supportFragmentManager.beginTransaction()

    FragmentTransaction transaction = supportFragmentManager.beginTransaction()

    transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
                R.anim.fade_in, R.anim.fade_out)

     */
    private void showAddStationFragment() {
        Fragment stationFragment = new AddStationFragment();
        Bundle argsBundle = new Bundle();
        stationFragment.setArguments(argsBundle);

        FragmentManager manager = getSupportFragmentManager();
        if (!firstLoaded) {
            manager.beginTransaction()
                    .replace(R.id.fragment_main_container, stationFragment)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)

                    .commit();
            firstLoaded = true;
        } else {
            manager.beginTransaction()
                    .replace(R.id.fragment_main_container, stationFragment)
                    .addToBackStack(null)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .commit();
        }
    }

    private void showWacthListFragment() {
        Fragment watchListFragment = new AirportWatchListFragment();
        Bundle argsBundle = new Bundle();
        watchListFragment.setArguments(argsBundle);
        FragmentManager manager = getSupportFragmentManager();

        if (!firstLoaded) {
            manager.beginTransaction()
                    .replace(R.id.fragment_main_container, watchListFragment)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .commit();
            firstLoaded = true;
        } else {
            manager.beginTransaction()
                    .replace(R.id.fragment_main_container, watchListFragment)
                    .addToBackStack(null)
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                    .commit();
        }
    }


    private void showNoStationsSelected() {
        Fragment noStationsFragment = new NoStationsSelectedFragment();
        Bundle argsBundle = new Bundle();
        noStationsFragment.setArguments(argsBundle);
        FragmentManager manager = getSupportFragmentManager();

        manager.beginTransaction()
                .replace(R.id.fragment_main_container, noStationsFragment)
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .commit();
        firstLoaded = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ViewGroup container = findViewById(R.id.fragment_main_container);

        switch (item.getItemId()) {
            case R.id.action_add_station:
                if (null != container) {
                    container.removeAllViews();
                }
                showAddStationFragment();
                return true;


            case R.id.action_watch_list:
                if (null != container) {
                    container.removeAllViews();
                }
                showWacthListFragment();
                return true;

            // TODO Done - Comment out for production version
            // There must be a better way to do this...
//            case R.id.action_crash_test:
//                if (BuildConfig.DEBUG) {
//                    Crashlytics.getInstance().crash(); // Force a crash
//                }
//                return true;

            case R.id.action_exit:
                // Close the app
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onPause() {
        if (null != mAdView) {
            mAdView.pause();
        }
        super.onPause();
    }


    @Override
    protected void onResume() {
        if (null != mAdView) {
            mAdView.resume();
        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (null != mAdView) {
            mAdView.destroy();
        }
        super.onDestroy();
    }


    // Method for notifying user of database initialization
    @Override
    public void onImportStart() {
        this.pbMainLoading.setVisibility(View.VISIBLE);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Done - Move text message to string resource
                Toast.makeText(getApplicationContext(), R.string.initializing_database_message, Toast.LENGTH_LONG).show();
            }
        });
    }

    // Not using this as the callbacks return
    // Immediately due to the DB and csv parsing
    // running on different thread. But it's
    // required to be here in the interface and
    // I may yet find a way to used it.
    @Override
    public void onImportFinished() {
        this.pbMainLoading.setVisibility(View.INVISIBLE);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(getApplicationContext(), "Airport Loading Complete.", Toast.LENGTH_LONG).show();
            }
        });
    }

    // MainActivity transitions
    @Override public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override public void finish() {
        super.finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
