/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/29/18 4:16 PM
 */

package us.sensornet.aviationweatherv2.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import us.sensornet.aviationweatherv2.R;


public class NoStationsSelectedFragment extends Fragment {

    public NoStationsSelectedFragment() {
        // Required empty public constructor
    }


    public static NoStationsSelectedFragment newInstance() {
        NoStationsSelectedFragment fragment = new NoStationsSelectedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_no_station_selected, container, false);
    }


}
