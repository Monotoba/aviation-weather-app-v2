/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 6:01 AM
 */

package us.sensornet.aviationweatherv2.ui.adapter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.ui.WatchItemDetailFragment;
import us.sensornet.aviationweatherv2.ui.view.DirectionalIndicator;
import us.sensornet.aviationweatherv2.utils.FlightRulesToColor;


public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.ViewHolder> {
    private static final String TAG = "WatchListAdapter";


    private List<MetarReport> mWatchedAirports = new ArrayList<>();

    private Context mContext;

    private Activity parentActivity;

    private AirportEntityAdapter.OnItemClickListener mListener;

    private ViewGroup mParentView;

    private FlightRulesToColor colorFlightRules;

    private int ScrollPos;


    public WatchListAdapter(Context context) {
        mContext = context;
        colorFlightRules = new FlightRulesToColor((Application) mContext.getApplicationContext());
    }

    public void setData(List<MetarReport> reports) {
        this.mWatchedAirports = new ArrayList<>();
        this.mWatchedAirports.addAll(reports);
        notifyDataSetChanged();
    }


    public MetarReport getStation(int position) {
        if(mWatchedAirports.size() > position) {
            return mWatchedAirports.get(position);
        } else {
            Log.d(TAG, "getStationAt: index: " + position
                    + " does not exists. We only have "
                    + String.valueOf(mWatchedAirports.size())
                    + " items");
            return null;
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mParentView = viewGroup;
        // create a new view
        ViewGroup itemView = (ViewGroup) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_watch_list_item, viewGroup, false);

        WatchListAdapter.ViewHolder mHolder = new WatchListAdapter.ViewHolder(itemView);

        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int pos) {

        boolean hasData = false;

        if(mWatchedAirports.size() > pos) {
            if (mWatchedAirports.get(pos).getError() != null) {
                // handle error returned
                String error = mWatchedAirports.get(pos).getError();
                Log.d(TAG, "onBindViewHolder: Error: " +
                        mWatchedAirports.get(pos).getError());
                if (mWatchedAirports.get(pos).getIcao().length() > 0) {
                    Log.d(TAG, "onBindViewHolder: Error Station Name: " + mWatchedAirports.get(pos).getStation().getName());
                    String icao = mWatchedAirports.get(pos).getIcao().replace("\"", "").trim();

                    // TODO Done - Move No Data Available to a string resource
                    viewHolder.tvAirportName.setText(R.string.no_data_available);
                    viewHolder.tvAirportCode.setText(icao);
                }
                String icao = mWatchedAirports.get(pos).getIcao();

            } else {
                // Display the data returned
                String name;
                if(mWatchedAirports.get(pos).getStation().getName() != null) {
                    name = mWatchedAirports.get(pos).getStation().getName().replace("\"", "").trim();
                } else {
                    name = "Not provided";
                }

                String icao;
                if(mWatchedAirports.get(pos).getIcao() != null) {
                    icao = mWatchedAirports.get(pos).getIcao();
                } else {
                    icao = "Not provided";
                }

                String flightCategory;
                if(mWatchedAirports.get(pos).getFlightCategory() != null) {
                    flightCategory = mWatchedAirports.get(pos).getFlightCategory();
                } else {
                    flightCategory = "Not provided";
                }

                String windDegrees = "";
                String windSpeed = "";
                String windSpeedUnit = "";


                if (null != mWatchedAirports.get(pos).getWind().getSpeedMph() &&
                        !mWatchedAirports.get(pos).getWind().getSpeedMph().toString().isEmpty()) {
                    windSpeed = String.valueOf(mWatchedAirports.get(pos).getWind().getSpeedKts());
                    windSpeed = windSpeed == null ? "" : windSpeed + " ";

                    windSpeedUnit = viewHolder.itemView.getContext().getString(R.string.knots_abbreviation);

                    hasData = true;
                }


                if (null != mWatchedAirports.get(pos).getWind().getDegrees()
                        && null != mWatchedAirports.get(pos).getWind().getSpeedKts()) {
                    windDegrees = String.valueOf(mWatchedAirports.get(pos).getWind().getDegrees());
                    windDegrees = windDegrees == null ? "" : windDegrees;
                    hasData = true;
                }


                String pressure = null;
                String pressureUnit = null;
                if (null != mWatchedAirports.get(pos).getBarometer().getKpa()) {
                    pressure = String.valueOf(mWatchedAirports.get(pos).getBarometer().getKpa().longValue());
                    // TODO Done - Move Hg to string resource
                    pressureUnit = viewHolder.itemView.getContext().getString(R.string.mercury_abbreviation);
                    pressure = (pressure == null ? "" : pressure.trim());
                    hasData = true;
                }


                String temperature = null;
                String temperatureUnits = null;
                if (null != mWatchedAirports.get(pos).getTemperature()) {
                    temperature = String.valueOf(mWatchedAirports.get(pos).getTemperature().getFahrenheit().longValue());
                    temperature = temperature == null ? "" : temperature.trim();
                    // TODO Done - Move F/C to string resource
                    temperatureUnits = viewHolder.itemView.getContext().getString(R.string.fahrenheit);
                    hasData = true;
                }

                String cloudCover = null;
                String baseFeetAgl = null;
                if (null != mWatchedAirports.get(pos).getClouds()) {
                    cloudCover = mWatchedAirports.get(pos).getClouds().get(0).getText();
                    baseFeetAgl = String.valueOf(mWatchedAirports.get(pos).getClouds().get(0).getBaseFeetAgl());
                    // TODO Done - Move Clouds, @, and Ft. to string resource
                    cloudCover = cloudCover == null ? "" : (cloudCover =
                            viewHolder.itemView.getContext().getString(R.string.clouds)
                                    + " " + cloudCover + " " + viewHolder.itemView.getContext()
                                    .getString(R.string.altitude_character)
                                    + baseFeetAgl + " " + viewHolder.itemView.getContext()
                                    .getString(R.string.feet_abbreviation));
                    hasData = true;
                }


                String visibility = null;
                if (null != mWatchedAirports.get(pos).getVisibility()) {
                    visibility = mWatchedAirports.get(pos).getVisibility().getMiles();
                    visibility = visibility == null ? "" : visibility;
                    // TODO Done - Move Visibility min. and Miles/Kilometers to string resource
                    visibility = viewHolder.itemView.getContext().getString(R.string.visibility)
                            + " " + viewHolder.itemView.getContext().getString(R.string.minimum_abbreviation)
                            + viewHolder.itemView.getContext().getString(R.string.colon_separator)
                            + " " + visibility
                            + " " + viewHolder.itemView.getContext().getString(R.string.miles);
                    //"Visibility min. " + visibility.concat(" Miles");
                    hasData = true;
                }


                viewHolder.tvAirportName.setText(name);
                viewHolder.tvAirportCode.setText(icao);
                viewHolder.tvFlightRules.setText(flightCategory);
                viewHolder.tvFlightRules.setBackgroundColor(colorFlightRules.getColor(flightCategory));
                viewHolder.tvWindHeading.setText(windDegrees.trim());
                viewHolder.tvWindSpeed.setText(windSpeed);
                viewHolder.tvWindSpeedUnit.setText(windSpeedUnit);
                viewHolder.tvBarometricPressure.setText(pressure);
                viewHolder.tvPressureUnit.setText(pressureUnit);
                viewHolder.tvTemperature.setText(temperature);
                viewHolder.tvTemperatureUnit.setText(temperatureUnits);
                viewHolder.tvCloudCover.setText(cloudCover);
                viewHolder.tvVisibility.setText(visibility);
                viewHolder.tvReportAgeMinutes.setText(mWatchedAirports.get(pos).getObserved());
                //TODO Done - place "Observered at" text in resource file
                String observation = viewHolder.itemView.getContext().getString(R.string.observed_at)
                        + viewHolder.itemView.getContext().getString(R.string.colon_separator)
                        + " " + mWatchedAirports.get(pos).getObserved();
                viewHolder.tvObservationTime.setText(observation);

                if (!windDegrees.trim().isEmpty()) {
                    Log.d(TAG, "onBindViewHolder: windDegrees: " + windDegrees);
                    windDegrees = windDegrees.isEmpty() ? "" : windDegrees;
                    //if(!windDegrees.isEmpty()) {
                    viewHolder.imgWindDirection.setRotation(Integer.parseInt(windDegrees.trim()));
                    //}
                    hasData = true;
                }

                //        viewHolder.imgReportAgeIndicator;
                if (null != mWatchedAirports.get(pos).getObserved()) {
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                    String strDate = mWatchedAirports.get(pos).getObserved();
                    strDate = strDate.replace("@", "");
                    strDate = strDate.replace("Z", "");
                    int ageInMinutes = 0;
                    try {

                        Date systemDate = Calendar.getInstance().getTime();
                        Date reportDate = format.parse(strDate);

                        //Log.d(TAG, "onBindViewHolder: systemDate: " + systemDate.toString());
                        //Log.d(TAG, "onBindViewHolder: reportDate: " + reportDate.toString());

                        long millss = systemDate.getTime();
                        long millsr = reportDate.getTime();
                        long mills = millss - millsr;

                        //Log.d(TAG, "onBindViewHolder: Date systemMillies: " + String.valueOf(millss));
                        //Log.d(TAG, "onBindViewHolder: Date reportMillies: " + String.valueOf(millsr));
                        //Log.d(TAG, "onBindViewHolder: Date systemMillies - reportMillies = " + Long.toString((millss - millsr)));

                        long ageInMinuts = mills; //(mills / (1000 * 60)) % 60;

                        //Log.d(TAG, "onBindViewHolder: Date ageInMinutes: " + Long.toString(ageInMinutes));
                        String msg = Long.toString(ageInMinutes);
                        viewHolder.tvReportAgeMinutes.setText(msg);

                    } catch (Exception e) {

                        viewHolder.tvReportAgeMinutes.setText(mWatchedAirports.get(pos).getObserved());
                    }

                    hasData = true;
                }

            }
        } else {
            Log.d(TAG, "onBindViewHolder: index: " + pos + " is out of bounds");
            notifyDataSetChanged();
        }

        if (!hasData) {
            // TODO Done - Add text to string resource
            Toast.makeText(mContext, R.string.no_data_available, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public int getItemCount() {
        if (null == mWatchedAirports) {
            return 0;
        }
        return mWatchedAirports.size();
    }

    private void showDetailFragment(String icao, int position) {
        Fragment detailFragment = new WatchItemDetailFragment();
        Bundle argsBundle = new Bundle();
        argsBundle.putInt("POSITION", position);
        argsBundle.putString("ICAO", icao);
        detailFragment.setArguments(argsBundle);


        FragmentManager manager = ((FragmentActivity) mContext).getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.fragment_main_container, detailFragment)
                .addToBackStack(null)
                .commit();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAirportName;
        TextView tvAirportCode;
        TextView tvFlightRules;
        DirectionalIndicator imgWindDirection;
        TextView tvWindHeading;
        TextView tvWindSpeed;
        TextView tvWindSpeedUnit;
        TextView tvBarometricPressure;
        TextView tvPressureUnit;
        TextView tvTemperature;
        TextView tvTemperatureUnit;
        TextView tvCloudCover;
        ImageView imgReportAgeIndicator;
        TextView tvVisibility;
        TextView tvReportAgeMinutes;
        TextView tvObservationTime;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            // Save the item index
            itemView.setTag(R.string.ADAPTER_POSITION_KEY, getAdapterPosition());

            tvAirportName = itemView.findViewById(R.id.tv_airport_name);
            tvAirportCode = itemView.findViewById(R.id.tv_airport_code);
            tvFlightRules = itemView.findViewById(R.id.tv_flight_rules);
            imgWindDirection = itemView.findViewById(R.id.img_wind_direction);
            tvWindHeading = itemView.findViewById(R.id.tv_wind_heading);
            tvWindSpeed = itemView.findViewById(R.id.tv_wind_speed);
            tvWindSpeedUnit = itemView.findViewById(R.id.tv_wind_speed_unit);
            tvBarometricPressure = itemView.findViewById(R.id.tv_barometric_pressure);
            tvPressureUnit = itemView.findViewById(R.id.tv_barometric_pressure_unit);
            tvTemperature = itemView.findViewById(R.id.tv_temperature);
            tvTemperatureUnit = itemView.findViewById(R.id.tv_temp_unit);
            tvCloudCover = itemView.findViewById(R.id.tv_cloud_cover);
            imgReportAgeIndicator = itemView.findViewById(R.id.img_report_age_indicator);
            tvVisibility = itemView.findViewById(R.id.tv_visibility);
            tvReportAgeMinutes = itemView.findViewById(R.id.tv_report_age_minutes);
            tvObservationTime = itemView.findViewById(R.id.tv_observation_time);

            // Open weather detail view on click
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (null == mWatchedAirports.get(position).getError()) {
                        String Icao = tvAirportCode.getText().toString();
                        showDetailFragment(Icao, position);
                    } else {
                        // If we are here we have an error. So handle it...
                        Toast.makeText(mContext, mWatchedAirports.get(position).getError(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
