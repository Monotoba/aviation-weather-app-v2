/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 11/10/18 3:33 PM
 */

package us.sensornet.aviationweatherv2.ui.adapter;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import us.sensornet.aviationweatherv2.R;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;
import us.sensornet.aviationweatherv2.service.AppWidgetService;

public class AviationWeatherAppWidgetProvider extends AppWidgetProvider {
    private static final String TAG = "AviationWeatherAppWidge";

    public static final String ACTION_LAUNCH = "us.sensornet.aviationweatherv2.ACTION_LAUNCH";
    public static final String EXTRA_ITEM_POSITION = "us.sensornet.aviationweatherv2.EXTRA_ITEM_POSITION";
    public static final String EXTRA_ITEM_ICAO_CODE = "us.sensornet.aviationweatherv2.EXTRA_ITEM_ICAO_CODE";
    public static final String EXTRA_DATA = "us.sensornet.aviationweatherv2.EXTRA_APPWIDGET_DATE";

    public static MetarReport[] mReports;

    public static String jsonString;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for(int appWidgetId:appWidgetIds){
            // Call service
            Intent serviceIntent = new Intent(context, AppWidgetService.class);
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            serviceIntent.putExtra(EXTRA_DATA, jsonString);
            serviceIntent.setData(Uri.parse(serviceIntent.toUri(Intent.URI_INTENT_SCHEME)));


            Intent clickIntent = new Intent(context, AviationWeatherAppWidgetProvider.class);
            clickIntent.setAction(ACTION_LAUNCH);
            PendingIntent clickPendingIntent = PendingIntent.getBroadcast(context,
                    0, clickIntent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.aviation_weather_app_widget);
            views.setRemoteAdapter(R.id.app_widget_stack_view, serviceIntent);
            views.setEmptyView(R.id.app_widget_stack_view, R.id.app_widget_empty_view);
            views.setPendingIntentTemplate(R.id.app_widget_stack_view, clickPendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, views);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.app_widget_stack_view);

        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }


}
