/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 2:49 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX;

import android.app.Application;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarContainer;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;

public class CheckWXAPIClient {
    private static final String TAG = "CheckWXAPIClient";

    private static final String baseUrl = "https://api.checkwx.com/";
    private static Retrofit retrofit = null;
    private static Application mApplication;
    private OkHttpClient.Builder okClient = null;

    //public static OkHttpClient getHttpClient(Application application) {
    public static OkHttpClient getHttpClient() {
        //mApplication = application;
        // Set logger
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Build Timed Cache
//        File httpCacheDirectory = new File(application.getCacheDir(), "http-cache");
//        int cacheSize = 10 * 1024 * 1024; // 10 MiB
//        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

//        httpClient.addNetworkInterceptor(new CacheInterceptor())
//            .cache(cache)
//            .build();

        OkHttpClient client = httpClient.build();
        return client;
    }


    public static Retrofit getClient() {

        if (null == retrofit) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(MetarContainer.class, new MetarContainerDeserializer())
                    .registerTypeAdapter(MetarReport.class, new MetarReportsDeserializer())
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }

        return retrofit;
    }


    public static class MetarReportDeserializer implements JsonDeserializer<MetarReport> {

        private final Gson gson = new Gson();

        @Override
        public MetarReport deserialize(JsonElement json, Type typeOfT
                , JsonDeserializationContext context)
                throws JsonParseException {
            try {
                return gson.fromJson(json, MetarReport.class);
            } catch (JsonSyntaxException e) {
                // it was not a Station object
                // so try to set the error string
                MetarReport report = new MetarReport();
                report.setError(json.getAsString());
                return report;
            }
        }

    }


    public static class MetarContainerDeserializer implements JsonDeserializer<MetarContainer> {
        private static final String TAG = "MetarContainerDeseriali";

        private final Gson gson = new Gson();

        @Override
        public MetarContainer deserialize(JsonElement json, Type typeOfT
                , JsonDeserializationContext context)
                throws JsonParseException {
            try {
                return gson.fromJson(json, MetarContainer.class);
            } catch (JsonSyntaxException e) {
                // it was not a MetarContainer object
                // so try to set the error string
                MetarContainer container = new MetarContainer();
                Log.d(TAG, "deserialize: json " + json.toString());
                if (json.toString().contains("<html>")) {
                    container.setError(json.getAsString());

                }

                return container;
            }
        }

    }


    public static class CacheIntercepter implements Interceptor {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            okhttp3.Response response = chain.proceed(chain.request());

            CacheControl cacheControl = new CacheControl.Builder()
                    .maxAge(15, TimeUnit.MINUTES) // 15 minutes cache
                    .build();

            return response.newBuilder()
                    .removeHeader("Pragma")
                    .removeHeader("Cache-Control")
                    .header("Cache-Control", cacheControl.toString())
                    .build();
        }
    }
}
