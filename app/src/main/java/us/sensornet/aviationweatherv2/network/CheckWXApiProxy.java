/*
 * Created By Randall L. Morgan
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/24/18 12:18 PM
 */

package us.sensornet.aviationweatherv2.network;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import us.sensornet.aviationweatherv2.database.AppDatabase;
import us.sensornet.aviationweatherv2.network.CheckWX.CheckWXApi;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarContainer;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;


/**
 * This class
 */
public class CheckWXApiProxy implements CheckWXApi.CheckWXAPIResults {
    private static final String TAG = "CheckWXApiProxy";

    private static CheckWXApiProxy INSTANCE;

    private AppDatabase mDatabase;

    private CheckWXApi mApi;


    private List<MetarReport> mMetarReports = new ArrayList<>();

    private MutableLiveData<ResponseEnvelope<List<MetarReport>>> responseEnvelope = new MutableLiveData<>();

    private Gson mGson = new Gson();


    private CheckWXResponseInterface mListener;

    private CheckWXApiProxy(Application application) {
        mDatabase = AppDatabase.getInstance(application, null);
        mApi = new CheckWXApi();
        // Register proxy as api listener
        mApi.addListener(this);
    }

    public static CheckWXApiProxy getInstance(Application application) {
        if (null == INSTANCE) {
            INSTANCE = new CheckWXApiProxy(application);
        }

        return INSTANCE;
    }

    public void setListener(CheckWXResponseInterface listener) {
        mListener = listener;
    }


    public LiveData<ResponseEnvelope<List<MetarReport>>> getMetarReport(String icao) {
        Log.d(TAG, "getMetarReport: called with icao: " + icao);
        mApi.getMetarReport(icao);
        return responseEnvelope;
    }


    @Override
    public void onMetarReportSuccess(MetarContainer container) {
        // First clear the list so we don't end up
        // with more items than we really have.
        mMetarReports.clear();
        if (null != container.getData()) {
            for (MetarReport report : container.getData()) {
                if (null == container.getData() || container.getData().isEmpty()) {
                    Log.d(TAG, "onMetarReportSuccess: container.getData() returned null or empty");
                } else {
                    Log.d(TAG, "onMetarReportSuccess: saving container data");
                    if (null == report.getError()) {
                        mMetarReports.add(report);
                    } else {
                        String[] parts = report.getError().split(" ");
                        if (parts[0].length() > 0) {
                            //report.setStation(parts[0]);
                            Log.d(TAG, "onMetarReportSuccess: " + parts[0]);

                            report.setIcao(parts[0]);
                            mMetarReports.add(report);
                        }
                    }
                }
            }
        }

        ResponseEnvelope<List<MetarReport>> envelope = new ResponseEnvelope<>();
        // wrap response in envelope and call repository
        envelope.setData(mMetarReports);

        // Update live data
        responseEnvelope.setValue(envelope);

        // Todo remove callback in favor of live data
        mListener.onMetarResponse(envelope);
    }

    @Override
    public void onMetarReportFailure(Throwable t) {
        ResponseEnvelope<List<MetarReport>> envelope = new ResponseEnvelope<>();
        Log.d(TAG, "onMetarReportFailure: called: " + t.getMessage() + "\nCaused BY: " + t.getCause());
        envelope.setApiException((Exception) t);

        // Update live data
        responseEnvelope.setValue(envelope);
        // TODO remove callback in favor of live data
        mListener.onMetarResponse(envelope);

    }

    // Callbacks use the ResponseEnvelope...
    public interface CheckWXResponseInterface {
        void onMetarResponse(ResponseEnvelope<List<MetarReport>> envelope);
    }
}
