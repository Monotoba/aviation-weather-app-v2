/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Dewpoint implements Serializable, Parcelable {

    public final static Parcelable.Creator<Dewpoint> CREATOR = new Creator<Dewpoint>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Dewpoint createFromParcel(Parcel in) {
            return new Dewpoint(in);
        }

        public Dewpoint[] newArray(int size) {
            return (new Dewpoint[size]);
        }

    };
    private final static long serialVersionUID = 3435174839766544787L;
    @SerializedName("celsius")
    @Expose
    private Integer celsius;
    @SerializedName("fahrenheit")
    @Expose
    private Integer fahrenheit;

    protected Dewpoint(Parcel in) {
        this.celsius = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.fahrenheit = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Dewpoint() {
    }

    /**
     * @param celsius
     * @param fahrenheit
     */
    public Dewpoint(Integer celsius, Integer fahrenheit) {
        super();
        this.celsius = celsius;
        this.fahrenheit = fahrenheit;
    }

    public Integer getCelsius() {
        return celsius;
    }

    public void setCelsius(Integer celsius) {
        this.celsius = celsius;
    }

    public Integer getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(Integer fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("celsius", celsius).append("fahrenheit", fahrenheit).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(celsius);
        dest.writeValue(fahrenheit);
    }

    public int describeContents() {
        return 0;
    }

}
