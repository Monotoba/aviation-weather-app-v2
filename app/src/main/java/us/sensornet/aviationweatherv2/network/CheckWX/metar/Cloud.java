
/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Cloud implements Serializable, Parcelable {

    public final static Parcelable.Creator<Cloud> CREATOR = new Creator<Cloud>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Cloud createFromParcel(Parcel in) {
            return new Cloud(in);
        }

        public Cloud[] newArray(int size) {
            return (new Cloud[size]);
        }

    };
    private final static long serialVersionUID = -4223130772596726527L;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("base_feet_agl")
    @Expose
    private Integer baseFeetAgl;
    @SerializedName("base_meters_agl")
    @Expose
    private Double baseMetersAgl;

    protected Cloud(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.baseFeetAgl = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.baseMetersAgl = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Cloud() {
    }

    /**
     * @param text
     * @param baseFeetAgl
     * @param baseMetersAgl
     * @param code
     */
    public Cloud(String code, String text, Integer baseFeetAgl, Double baseMetersAgl) {
        super();
        this.code = code;
        this.text = text;
        this.baseFeetAgl = baseFeetAgl;
        this.baseMetersAgl = baseMetersAgl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getBaseFeetAgl() {
        return baseFeetAgl;
    }

    public void setBaseFeetAgl(Integer baseFeetAgl) {
        this.baseFeetAgl = baseFeetAgl;
    }

    public Double getBaseMetersAgl() {
        return baseMetersAgl;
    }

    public void setBaseMetersAgl(Double baseMetersAgl) {
        this.baseMetersAgl = baseMetersAgl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", code).append("text", text).append("baseFeetAgl", baseFeetAgl).append("baseMetersAgl", baseMetersAgl).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(text);
        dest.writeValue(baseFeetAgl);
        dest.writeValue(baseMetersAgl);
    }

    public int describeContents() {
        return 0;
    }

}