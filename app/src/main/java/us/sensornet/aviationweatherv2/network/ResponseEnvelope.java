/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/24/18 11:50 AM
 */

package us.sensornet.aviationweatherv2.network;

import android.arch.lifecycle.LiveData;

import java.util.List;

import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;

/**
 * Class ResponseEnvelope<T>
 * This class is used to wrap network response
 * that may be either data or Exception. This
 * response of two different types presents an
 * issue with passing data back up to the view
 * model, as it should expect a single return
 * type per request. Providing a data wrapper
 * or envelope, allows us to wrap either
 * response in a single data type. The view
 * model can then query the response envelope
 * and determine if an error occured or if
 * useful data exists and respond appropriately.
 *
 * @param <T>
 */
public class ResponseEnvelope<T> extends LiveData<ResponseEnvelope<List<MetarReport>>> {
    private Exception apiException;
    private T data;
    private boolean isError = false;

    public ResponseEnvelope() {
    }

    public Exception getApiException() {
        return apiException;
    }

    public void setApiException(Exception apiException) {
        this.apiException = apiException;
        this.isError = true;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean getIsError() {
        return isError;
    }

    public void setIsError(boolean state) {
        isError = state;
    }
}
