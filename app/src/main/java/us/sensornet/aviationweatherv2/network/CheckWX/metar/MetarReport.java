/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class MetarReport implements Serializable, Parcelable {

    public final static Parcelable.Creator<MetarReport> CREATOR = new Creator<MetarReport>() {
        @SuppressWarnings({
                "unchecked"
        })
        public MetarReport createFromParcel(Parcel in) {
            return new MetarReport(in);
        }

        public MetarReport[] newArray(int size) {
            return (new MetarReport[size]);
        }
    };
    private final static long serialVersionUID = -8763715221889676178L;
    @SerializedName("icao")
    @Expose
    private String icao;
    @SerializedName("station")
    @Expose
    private Station station;
    @SerializedName("observed")
    @Expose
    private String observed;
    @SerializedName("raw_text")
    @Expose
    private String rawText;
    @SerializedName("barometer")
    @Expose
    private Barometer barometer;
    @SerializedName("ceiling")
    @Expose
    private Ceiling ceiling;
    @SerializedName("clouds")
    @Expose
    private List<Cloud> clouds = null;
    @SerializedName("dewpoint")
    @Expose
    private Dewpoint dewpoint;
    @SerializedName("elevation")
    @Expose
    private Elevation elevation;
    @SerializedName("flight_category")
    @Expose
    private String flightCategory;
    @SerializedName("humidity_percent")
    @Expose
    private Integer humidityPercent;
    @SerializedName("temperature")
    @Expose
    private Temperature temperature;
    @SerializedName("visibility")
    @Expose
    private Visibility visibility;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("rain_in")
    @Expose
    private Double rainIn;
    @SerializedName("snow_in")
    @Expose
    private Double snowIn;
    @SerializedName("conditions")
    @Expose
    private List<Condition> conditions = null;
    @SerializedName("additionalProperties")
    @Expose
    private List<String> additionalProperties = null;
    @SerializedName("error")
    @Expose
    private String error;

    protected MetarReport(Parcel in) {
        this.icao = ((String) in.readValue((String.class.getClassLoader())));
        this.station = (Station) in.readValue((Station.class.getClassLoader()));
        this.observed = ((String) in.readValue((String.class.getClassLoader())));
        this.rawText = ((String) in.readValue((String.class.getClassLoader())));
        this.barometer = ((Barometer) in.readValue((Barometer.class.getClassLoader())));
        this.ceiling = ((Ceiling) in.readValue((Ceiling.class.getClassLoader())));
        in.readList(this.clouds, (Cloud.class.getClassLoader()));
        this.dewpoint = ((Dewpoint) in.readValue((Dewpoint.class.getClassLoader())));
        this.elevation = ((Elevation) in.readValue((Elevation.class.getClassLoader())));
        this.flightCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.humidityPercent = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.temperature = ((Temperature) in.readValue((Temperature.class.getClassLoader())));
        this.visibility = ((Visibility) in.readValue((Visibility.class.getClassLoader())));
        this.wind = ((Wind) in.readValue((Wind.class.getClassLoader())));
        in.readList(this.conditions, (Condition.class.getClassLoader()));
        this.rainIn = ((Double) in.readValue((String.class.getClassLoader())));
        this.snowIn = ((Double) in.readValue((String.class.getClassLoader())));
        in.readList(this.additionalProperties, (java.lang.String.class.getClassLoader()));
        this.error = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public MetarReport() {
    }

    /**
     * @param clouds
     * @param wind
     * @param rawText
     * @param flightCategory
     * @param visibility
     * @param elevation
     * @param humidityPercent
     * @param temperature
     * @param observed
     * @param ceiling
     * @param icao
     * @param station
     * @param dewpoint
     * @param barometer
     * @param conditions
     * @param rainIn
     * @param snowIn
     */
    public MetarReport(String icao, Station station, String observed, String rawText,
                       Barometer barometer, Ceiling ceiling, List<Cloud> clouds,
                       Dewpoint dewpoint, Elevation elevation, String flightCategory,
                       Integer humidityPercent, Temperature temperature, Visibility visibility,
                       Wind wind, List<Condition> conditions, Double rainIn, Double snowIn,
                       List<String> additionalProperties, String error) {
        super();
        this.icao = icao;
        this.station = (Station) station;
        this.observed = observed;
        this.rawText = rawText;
        this.barometer = barometer;
        this.ceiling = ceiling;
        this.clouds = clouds;
        this.dewpoint = dewpoint;
        this.elevation = elevation;
        this.flightCategory = flightCategory;
        this.humidityPercent = humidityPercent;
        this.temperature = temperature;
        this.visibility = visibility;
        this.wind = wind;
        this.conditions = conditions;
        this.rainIn = rainIn;
        this.snowIn = snowIn;
        this.additionalProperties = additionalProperties;
        this.error = error;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getObserved() {
        return observed;
    }

    public void setObserved(String observed) {
        this.observed = observed;
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public Barometer getBarometer() {
        return barometer;
    }

    public void setBarometer(Barometer barometer) {
        this.barometer = barometer;
    }

    public Ceiling getCeiling() {
        return ceiling;
    }

    public void setCeiling(Ceiling ceiling) {
        this.ceiling = ceiling;
    }

    public List<Cloud> getClouds() {
        return clouds;
    }

    public void setClouds(List<Cloud> clouds) {
        this.clouds = clouds;
    }

    public Dewpoint getDewpoint() {
        return dewpoint;
    }

    public void setDewpoint(Dewpoint dewpoint) {
        this.dewpoint = dewpoint;
    }

    public Elevation getElevation() {
        return elevation;
    }

    public void setElevation(Elevation elevation) {
        this.elevation = elevation;
    }

    public String getFlightCategory() {
        return flightCategory;
    }

    public void setFlightCategory(String flightCategory) {
        this.flightCategory = flightCategory;
    }

    public Integer getHumidityPercent() {
        return humidityPercent;
    }

    public void setHumidityPercent(Integer humidityPercent) {
        this.humidityPercent = humidityPercent;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Double getRainIn() {
        return rainIn;
    }

    public void setRainIn(Double rainIn) {
        this.rainIn = rainIn;
    }

    public Double getSnowIn() {
        return snowIn;
    }

    public void setSnowIn(Double snowIn) {
        this.snowIn = snowIn;
    }

    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public List<String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(List<String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("icao", icao)
                .append("station", station).append("observed", observed)
                .append("rawText", rawText).append("barometer", barometer)
                .append("ceiling", ceiling).append("clouds", clouds)
                .append("dewpoint", dewpoint).append("elevation", elevation)
                .append("flightCategory", flightCategory)
                .append("humidityPercent", humidityPercent)
                .append("temperature", temperature).append("visibility", visibility)
                .append("wind", wind).append("conditions", conditions)
                .append("rainIn", rainIn).append("snowIn", snowIn)
                .append("additionalProperties", additionalProperties)
                .append("error", error).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(icao);
        dest.writeValue(station);
        dest.writeValue(observed);
        dest.writeValue(rawText);
        dest.writeValue(barometer);
        dest.writeValue(ceiling);
        dest.writeList(clouds);
        dest.writeValue(dewpoint);
        dest.writeValue(elevation);
        dest.writeValue(flightCategory);
        dest.writeValue(humidityPercent);
        dest.writeValue(temperature);
        dest.writeValue(visibility);
        dest.writeValue(wind);
        dest.writeValue(conditions);
        dest.writeValue(rainIn);
        dest.writeValue(snowIn);
        dest.writeList(additionalProperties);
    }

    public int describeContents() {
        return 0;
    }

}