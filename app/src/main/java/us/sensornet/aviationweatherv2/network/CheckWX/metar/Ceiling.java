
/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Ceiling implements Serializable, Parcelable {

    public final static Parcelable.Creator<Ceiling> CREATOR = new Creator<Ceiling>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Ceiling createFromParcel(Parcel in) {
            return new Ceiling(in);
        }

        public Ceiling[] newArray(int size) {
            return (new Ceiling[size]);
        }

    };
    private final static long serialVersionUID = 1865543316267340402L;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("feet_agl")
    @Expose
    private Integer feetAgl;
    @SerializedName("meters_agl")
    @Expose
    private Double metersAgl;

    protected Ceiling(Parcel in) {
        this.code = ((String) in.readValue((String.class.getClassLoader())));
        this.text = ((String) in.readValue((String.class.getClassLoader())));
        this.feetAgl = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.metersAgl = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Ceiling() {
    }

    /**
     * @param text
     * @param feetAgl
     * @param code
     * @param metersAgl
     */
    public Ceiling(String code, String text, Integer feetAgl, Double metersAgl) {
        super();
        this.code = code;
        this.text = text;
        this.feetAgl = feetAgl;
        this.metersAgl = metersAgl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getFeetAgl() {
        return feetAgl;
    }

    public void setFeetAgl(Integer feetAgl) {
        this.feetAgl = feetAgl;
    }

    public Double getMetersAgl() {
        return metersAgl;
    }

    public void setMetersAgl(Double metersAgl) {
        this.metersAgl = metersAgl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", code).append("text", text).append("feetAgl", feetAgl).append("metersAgl", metersAgl).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(text);
        dest.writeValue(feetAgl);
        dest.writeValue(metersAgl);
    }

    public int describeContents() {
        return 0;
    }

}
