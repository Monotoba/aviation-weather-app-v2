/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Elevation implements Serializable, Parcelable {

    public final static Parcelable.Creator<Elevation> CREATOR = new Creator<Elevation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Elevation createFromParcel(Parcel in) {
            return new Elevation(in);
        }

        public Elevation[] newArray(int size) {
            return (new Elevation[size]);
        }

    };
    private final static long serialVersionUID = 5031154724018809061L;
    @SerializedName("feet")
    @Expose
    private Integer feet;
    @SerializedName("meters")
    @Expose
    private Integer meters;

    protected Elevation(Parcel in) {
        this.feet = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.meters = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Elevation() {
    }

    /**
     * @param meters
     * @param feet
     */
    public Elevation(Integer feet, Integer meters) {
        super();
        this.feet = feet;
        this.meters = meters;
    }

    public Integer getFeet() {
        return feet;
    }

    public void setFeet(Integer feet) {
        this.feet = feet;
    }

    public Integer getMeters() {
        return meters;
    }

    public void setMeters(Integer meters) {
        this.meters = meters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("feet", feet).append("meters", meters).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(feet);
        dest.writeValue(meters);
    }

    public int describeContents() {
        return 0;
    }

}