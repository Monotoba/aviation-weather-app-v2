/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class MetarContainer implements Serializable, Parcelable {

    public final static Parcelable.Creator<MetarContainer> CREATOR = new Creator<MetarContainer>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MetarContainer createFromParcel(Parcel in) {
            return new MetarContainer(in);
        }

        public MetarContainer[] newArray(int size) {
            return (new MetarContainer[size]);
        }

    };
    private final static long serialVersionUID = 3499845587277497781L;
    @SerializedName("results")
    @Expose
    private Integer results;
    @SerializedName("data")
    @Expose
    private List<MetarReport> data = null;
    @SerializedName("additionalProperties")
    @Expose
    private List<String> additionalProperties = null;
    @SerializedName("error")
    @Expose
    private String error;

    protected MetarContainer(Parcel in) {
        this.results = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.data, (MetarReport.class.getClassLoader()));
        in.readList(this.additionalProperties, (java.lang.String.class.getClassLoader()));

    }

    /**
     * No args constructor for use in serialization
     */
    public MetarContainer() {
    }

    /**
     * @param results
     * @param data
     */
    public MetarContainer(Integer results, List<MetarReport> data, List<String> additionalProperties) {
        super();
        this.results = results;
        this.data = data;
        this.additionalProperties = additionalProperties;
    }

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<MetarReport> getData() {
        return data;
    }

    public void setData(List<MetarReport> data) {
        this.data = data;
    }

    public List<String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(List<String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("results", results)
                .append("data", data)
                .append("additionalProperties" + additionalProperties)
                .append("error", error).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(results);
        dest.writeList(data);
        dest.writeValue(additionalProperties);
        dest.writeValue(error);
    }

    public int describeContents() {
        return 0;
    }

}