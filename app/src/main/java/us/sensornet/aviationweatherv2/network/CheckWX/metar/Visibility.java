/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Visibility implements Serializable, Parcelable {

    public final static Parcelable.Creator<Visibility> CREATOR = new Creator<Visibility>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Visibility createFromParcel(Parcel in) {
            return new Visibility(in);
        }

        public Visibility[] newArray(int size) {
            return (new Visibility[size]);
        }

    };
    private final static long serialVersionUID = 6493656008960855511L;
    @SerializedName("miles")
    @Expose
    private String miles;
    @SerializedName("meters")
    @Expose
    private String meters;

    protected Visibility(Parcel in) {
        this.miles = ((String) in.readValue((String.class.getClassLoader())));
        this.meters = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Visibility() {
    }

    /**
     * @param meters
     * @param miles
     */
    public Visibility(String miles, String meters) {
        super();
        this.miles = miles;
        this.meters = meters;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getMeters() {
        return meters;
    }

    public void setMeters(String meters) {
        this.meters = meters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("miles", miles).append("meters", meters).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(miles);
        dest.writeValue(meters);
    }

    public int describeContents() {
        return 0;
    }

}