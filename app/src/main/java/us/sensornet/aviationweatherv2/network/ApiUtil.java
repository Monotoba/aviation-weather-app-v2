/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 3:06 PM
 */

package us.sensornet.aviationweatherv2.network;

import us.sensornet.aviationweatherv2.network.CheckWX.CheckWXAPIClient;
import us.sensornet.aviationweatherv2.network.CheckWX.CheckWXAPIService;

public class ApiUtil {

    public static CheckWXAPIService getCheckWXService() {
        return CheckWXAPIClient.getClient().create(CheckWXAPIService.class);
    }
}
