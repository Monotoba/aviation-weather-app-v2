/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Wind implements Serializable, Parcelable {
    public final static Parcelable.Creator<Wind> CREATOR = new Creator<Wind>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Wind createFromParcel(Parcel in) {
            return new Wind(in);
        }

        public Wind[] newArray(int size) {
            return (new Wind[size]);
        }

    };
    private static final String TAG = "Wind";
    private final static long serialVersionUID = -7865823069158415682L;
    @SerializedName("degrees")
    @Expose
    private Integer degrees;
    @SerializedName("speed_kts")
    @Expose
    private Integer speedKts;
    @SerializedName("speed_mph")
    @Expose
    private Integer speedMph;
    @SerializedName("speed_mps")
    @Expose
    private Integer speedMps;
    @SerializedName("gust_kts")
    @Expose
    private Integer gustKts;
    @SerializedName("gust_mph")
    @Expose
    private Integer gustMph;
    @SerializedName("gust_mps")
    @Expose
    private Integer gustMps;


    protected Wind(Parcel in) {
        this.degrees = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.speedKts = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.speedMph = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.speedMps = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.gustKts = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.gustMph = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.gustMps = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Wind() {
    }

    /**
     * @param gustKts
     * @param speedKts
     * @param speedMps
     * @param gustMps
     * @param degrees
     * @param speedMph
     * @param gustMph
     */
    public Wind(Integer degrees, Integer speedKts, Integer speedMph, Integer speedMps, Integer gustKts, Integer gustMph, Integer gustMps) {
        super();
        this.degrees = degrees;
        this.speedKts = speedKts;
        this.speedMph = speedMph;
        this.speedMps = speedMps;
        this.gustKts = gustKts;
        this.gustMph = gustMph;
        this.gustMps = gustMps;
    }

    public Integer getDegrees() {
        return degrees;
    }

    public void setDegrees(Integer degrees) {
        this.degrees = degrees;
    }

    public Integer getSpeedKts() {
        return speedKts;
    }

    public void setSpeedKts(Integer speedKts) {
        this.speedKts = speedKts;
    }

    public Integer getSpeedMph() {
        return speedMph;
    }

    public void setSpeedMph(Integer speedMph) {
        this.speedMph = speedMph;
    }

    public Integer getSpeedMps() {
        return speedMps;
    }

    public void setSpeedMps(Integer speedMps) {
        this.speedMps = speedMps;
    }

    public Integer getGustKts() {
        return gustKts;
    }

    public void setGustKts(Integer gustKts) {
        this.gustKts = gustKts;
    }

    public Integer getGustMph() {
        return gustMph;
    }

    public void setGustMph(Integer gustMph) {
        this.gustMph = gustMph;
    }

    public Integer getGustMps() {
        return gustMps;
    }

    public void setGustMps(Integer gustMps) {
        this.gustMps = gustMps;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("degrees", degrees).append("speedKts", speedKts).append("speedMph", speedMph).append("speedMps", speedMps).append("gustKts", gustKts).append("gustMph", gustMph).append("gustMps", gustMps).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(degrees);
        dest.writeValue(speedKts);
        dest.writeValue(speedMph);
        dest.writeValue(speedMps);
        dest.writeValue(gustKts);
        dest.writeValue(gustMph);
        dest.writeValue(gustMps);
    }

    public int describeContents() {
        return 0;
    }

}