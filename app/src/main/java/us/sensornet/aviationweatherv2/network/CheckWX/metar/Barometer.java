
/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 1:59 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Barometer implements Serializable, Parcelable {

    public final static Parcelable.Creator<Barometer> CREATOR = new Creator<Barometer>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Barometer createFromParcel(Parcel in) {
            return new Barometer(in);
        }

        public Barometer[] newArray(int size) {
            return (new Barometer[size]);
        }

    };
    private final static long serialVersionUID = 1268450926408949636L;
    @SerializedName("hg")
    @Expose
    private Double hg;
    @SerializedName("kpa")
    @Expose
    private Double kpa;
    @SerializedName("mb")
    @Expose
    private Double mb;

    protected Barometer(Parcel in) {
        this.hg = ((Double) in.readValue((Double.class.getClassLoader())));
        this.kpa = ((Double) in.readValue((Double.class.getClassLoader())));
        this.mb = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Barometer() {
    }

    /**
     * @param kpa
     * @param mb
     * @param hg
     */
    public Barometer(Double hg, Double kpa, Double mb) {
        super();
        this.hg = hg;
        this.kpa = kpa;
        this.mb = mb;
    }

    public Double getHg() {
        return hg;
    }

    public void setHg(Double hg) {
        this.hg = hg;
    }

    public Double getKpa() {
        return kpa;
    }

    public void setKpa(Double kpa) {
        this.kpa = kpa;
    }

    public Double getMb() {
        return mb;
    }

    public void setMb(Double mb) {
        this.mb = mb;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("hg", hg).append("kpa", kpa).append("mb", mb).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(hg);
        dest.writeValue(kpa);
        dest.writeValue(mb);
    }

    public int describeContents() {
        return 0;
    }

}