/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 11/4/18 1:06 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarReport;

public class MetarReportsDeserializer implements JsonDeserializer<MetarReport> {

    private final Gson gson = new Gson();

    @Override
    public MetarReport deserialize(JsonElement json, Type typeOfT
            , JsonDeserializationContext context)
            throws JsonParseException {
        try {
            return gson.fromJson(json, MetarReport.class);
        } catch (JsonSyntaxException e) {
            // it was not a Station object
            MetarReport report = new MetarReport();
            // so try to set the error string
            report.setError(json.getAsString());
            return report;
        }
    }

}