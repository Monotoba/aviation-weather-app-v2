/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 8:17 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarContainer;

public class CheckWXApi {
    private static final String TAG = "CheckWXApi";

    private CheckWXAPIService mService = CheckWXAPIClient.getClient().create(CheckWXAPIService.class);

    private List<CheckWXAPIResults> mListeners = new ArrayList<>();

    private String mResponseBody = "";

    public CheckWXApi() {
        // Empty constructor
    }


    // Get MetarContainer Report for ICAO code
    public void getMetarReport(String icao) {

        mService.getMetar(icao).enqueue(new Callback<MetarContainer>() {
            @Override
            public void onResponse(Call<MetarContainer> call, Response<MetarContainer> response) {
                Log.d(TAG, "onResponse: " + response.body());
                Log.d(TAG, "onResponse: ResponseCode:" + response.code());
                MetarContainer metarReports = response.body();

                mResponseBody = response.body().toString();

                Log.d(TAG, "onResponse: response.raw(): " + response.raw());
                Log.d(TAG, "onResponse: error.body(): " + response.errorBody());
                Log.d(TAG, "onResponse: response.body(): " + response.body().toString());

                if (null == metarReports.getError()) {
                    Log.d(TAG, "onResponse: ================================");
                    Log.d(TAG, "onResponse: returned: " + metarReports.getResults()
                            + " Data: " + metarReports.getData());
                    Log.d(TAG, "onResponse: ================================");
                } else {
                    // We have error
                    Log.d(TAG, "onResponse Metar Container Error: " + metarReports.getError());
                }

                onMetarReportSuccess(metarReports);
            }

            @Override
            public void onFailure(Call<MetarContainer> call, Throwable t) {

                Log.d(TAG, "onFailure: " + t.getMessage() + ", " + t.getCause());
                Log.getStackTraceString(new Exception());
                Log.d(TAG, "onFailure: mResponseBody: " + mResponseBody);
                onMetarReportFailure(t);
            }
        });
    }


    // MetarContainer Report
    public void onMetarReportSuccess(MetarContainer report) {
        for (CheckWXAPIResults listener : mListeners) {
            listener.onMetarReportSuccess(report);
        }
    }

    public void onMetarReportFailure(Throwable t) {
        for (CheckWXAPIResults listener : mListeners) {
            listener.onMetarReportFailure(t);
        }
    }


    public void addListener(CheckWXAPIResults listener) {
        if (!mListeners.contains(listener)) {
            mListeners.add(listener);
        }
    }

    public void removeListener(CheckWXAPIResults listener) {
        if (mListeners.contains(listener)) {
            mListeners.remove(listener);
        }
    }


    public interface CheckWXAPIResults {
        void onMetarReportSuccess(MetarContainer metarReport);
        void onMetarReportFailure(Throwable t);
    }

}
