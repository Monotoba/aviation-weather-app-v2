/*
 * Created By Randall L. Morgan /
 * Randall Morgan <rmorgan62@gmail.com>
 * Copyright (c) 2018. All rights reserved.
 * Last modified 10/22/18 6:55 PM
 */

package us.sensornet.aviationweatherv2.network.CheckWX;

import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import us.sensornet.aviationweatherv2.network.CheckWX.metar.MetarContainer;

public interface CheckWXAPIService {
    @Headers("x-api-key: 0f528c30a5e8c1a4131e0bbb10")
    @GET("metar/{icao}/decoded")
    Call<MetarContainer> getMetar(@Path("icao") @NonNull String icao);
}
