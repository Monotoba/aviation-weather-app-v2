package us.sensornet.aviationweatherv2.network.CheckWX.metar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Station implements Serializable, Parcelable {

    public final static Parcelable.Creator<Station> CREATOR = new Parcelable.Creator<Station>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Station createFromParcel(Parcel in) {
            return new Station(in);
        }

        public Station[] newArray(int size) {
            return (new Station[size]);
        }

    };
    private final static long serialVersionUID = 1268451026408949636L;
    @SerializedName("name")
    @Expose
    private String name;


    protected Station(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));

    }

    /**
     * No args constructor for use in serialization
     */
    public Station() {
    }

    /**
     * @param name
     */
    public Station(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);

    }

    public int describeContents() {
        return 0;
    }

}
