package us.sensornet.aviationweatherv2;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import us.sensornet.aviationweatherv2.database.Airports.AirportDao;
import us.sensornet.aviationweatherv2.database.Airports.AirportEntity;
import us.sensornet.aviationweatherv2.database.AppDatabase;

import static org.mockito.Mockito.verify;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AirportDaoTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private AppDatabase Db;
    private AirportDao dao;

    @Mock
    private Observer<List<AirportEntity>> observer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Context context = InstrumentationRegistry.getTargetContext();
        Db = Room.inMemoryDatabaseBuilder(context, AppDatabase.class)
                .allowMainThreadQueries().build();
        dao = Db.airportDao();
    }

    @After
    public void tearDown() throws Exception {
        Db.close();
    }

    @Test
    public void insert() throws Exception {
        // given
        AirportEntity airportEntity01 = new AirportEntity(2434, "EGLL", "large_airport",
                "London Heathrow AirportEntity", "51.4706", "-0.461941",
                "83", "EU", "GB", "GB-ENG",
                "London", "1", "EGLL", "LHR",
                " ", "http://www.heathrowairport.com/",
                "https://en.wikipedia.org/wiki/Heathrow_Airport",
                "LON, Londres", "1251675", "2018-09-16T02:32:35+00:00",
                "2018-09-16T02:32:35+00:00");

        AirportEntity airportEntity02 = new AirportEntity(3632, "KLAX", "large_airport",
                "Los Angeles International AirportEntity", "33.94250107",
                "-118.4079971", "125", "NA", "US",
                "US-CA", "Los Angeles", "1", "KLAX",
                "LAX", "LAX", "http://www.iflylax.com/",
                "http://en.wikipedia.org/wiki/Los_Angeles_International_Airport",
                " ", "1335475", "2010-01-23T11:27:55+00:00",
                "2010-01-23T11:27:55+00:00");

        AirportEntity airportEntity03 = new AirportEntity(3754, "KORD", "large_airport",
                "Chicago O'Hare International AirportEntity", "41.9786",
                "-87.9048", "672", "NA", "US",
                "US-IL", "Chicago", "1", "KORD",
                "ORD", "ORD",
                "https://www.flychicago.com/ohare/home/pages/default.aspx",
                "http://en.wikipedia.org/wiki/O'Hare_International_Airport",
                "CHI, Orchard Place", "1503175",
                "2018-09-16T02:35:35+00:00",
                "2018-09-16T02:35:35+00:00");

        List<AirportEntity> airportEntities = new ArrayList<>();
        airportEntities.add(airportEntity01);
        airportEntities.add(airportEntity02);
        airportEntities.add(airportEntity03);

        dao.getMatching("%").observeForever(observer);

        // when
        dao.insert(airportEntities);

        // then
        verify(observer).onChanged(Collections.synchronizedList(airportEntities));
    }
}

/*
    @Test
    public void writeUserAndReadInList() throws Exception {
        User user = TestUtil.createUser(3);
        user.setName("george");
        mUserDao.insert(user);
        List<User> byName = mUserDao.findUsersByName("george");
        assertThat(byName.get(0), equalTo(user));
    }
}

 */