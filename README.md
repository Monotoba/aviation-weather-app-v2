# Aviation Weather App
## Capstone Project

This is a simple aviation weather application that implements the 
requirements set forth in the Udacity Android Developer Nano Degree 
Program. The app has been modified to fix an issue that arose when
an API provider changed their API implementation by renaming a 
guaranteed field. The renaming didn't just change the name of the 
field, it placed it inside a new object. So code had to be added to 
to parse the new object and retrieve the field. A poor thing to do
to what is supposed to be a stable API!

Here's the eye candy:

![picture](app/src/main/assets/screenshots/Screenshot_01.png)
![picture](app/src/main/assets/screenshots/Screenshot_02.png)


![picture](app/src/main/assets/screenshots/Screenshot_03.png)
![picture](app/src/main/assets/screenshots/Screenshot_04.png)


![picture](app/src/main/assets/screenshots/Screenshot_05.png)
![picture](app/src/main/assets/screenshots/Screenshot_06.png)


![picture](app/src/main/assets/screenshots/Screenshot_07.png)
![picture](app/src/main/assets/screenshots/Screenshot_08.png)


![picture](app/src/main/assets/screenshots/Screenshot_09.png)
![picture](app/src/main/assets/screenshots/Screenshot_10.png)


![picture](app/src/main/assets/screenshots/Screenshot_11.png)
![picture](app/src/main/assets/screenshots/Screenshot_12.png)


![picture](app/src/main/assets/screenshots/Screenshot_13.png)
![picture](app/src/main/assets/screenshots/Screenshot_14.png)


![picture](app/src/main/assets/screenshots/Screenshot_15.png)
![picture](app/src/main/assets/screenshots/Screenshot_16.png)
